<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php if (isset($_GET['debug'])) { ?> 
        <link rel="stylesheet" href="/script/css/reset.css"  />        
        <link rel="stylesheet" href="/script/css/perfect-scrollbar.css" />
        <link rel="stylesheet" href="/script/css/idangerous.swiper.css" />
        <link rel="stylesheet" href="/script/css/stylesheet.css" />        
        <link rel="stylesheet" href="/script/css/style.css?t=<?php echo time(); ?>"  />
        <link rel="stylesheet" href="/script/css/perfect-scrollbar.css" />
        <link rel="stylesheet" href="/script/css/jquery-ui.css" />
        <?php } else { 
            $t = time();
            echo '<link rel="stylesheet" href="/script/player.min.css?ver='.$t.'" />';
        } ?>                            
      
    </head>
    <body class="">           
        <?php if (isset($_GET['debug'])) { ?>                            
        <script src="/script/libs/jquery.js?t=<?php echo time(); ?>"></script>            
        <script src="/script/libs/json2.js?t=<?php echo time(); ?>"></script>            
        <script src="/script/libs/underscore.js?t=<?php echo time(); ?>"></script>            
        <script src="/script/libs/jquery-ui.js?t=<?php echo time(); ?>"></script>            
        <script src="/script/libs/backbone.js?t=<?php echo time(); ?>"></script>
        <script src="/script/libs/idangerous.swiper-2.1.min.js?t=<?php echo time(); ?>"></script>        
        <script src="/script/src/tbapp.js?t=<?php echo time(); ?>"></script>
        <script src="/script/src/translate.js?t=<?php echo time(); ?>"></script>
        <script src="/script/src/helpers.js?t=<?php echo time(); ?>"></script>                        
        <script src="/script/src/models.js?t=<?php echo time(); ?>"></script>        
        <script src="/script/src/server.js?t=<?php echo time(); ?>"></script>                        
        <script src="/script/src/login.js?t=<?php echo time(); ?>"></script>        
        <script src="/script/src/header.js?t=<?php echo time(); ?>"></script>                
        <script src="/script/src/main.js?t=<?php echo time(); ?>"></script>        
        <script src="/script/src/profile.js?t=<?php echo time(); ?>"></script>        
        <script src="/script/src/users.js?t=<?php echo time(); ?>"></script>        
		<script src="/script/src/watchlist.js?t=<?php echo time(); ?>"></script>        
        <script src="/script/src/controller.js?t=<?php echo time(); ?>"></script>          
        <script src="/script/src/resize.js?t=<?php echo time(); ?>"></script>                    
        <?php } else { 
            $t = time();
            echo '<script src="/script/player.min.js?ver='.$t.'"></script>';                        
        } ?>                                    
        <div id="main-container"></div>        
    </body>
</html>
