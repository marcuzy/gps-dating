<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
       
        'vk' => [
            'class' => 'app\components\Vk\Vk',
            'app_id' => '4805460',
            'api_secret' => 'hJcmJxnhgyY0PD7cNgy7',
            'scope' => 'friends'
        ],

        'fb' => [
            'class' => 'app\components\FacebookWrapper',
            'appId' => '1576187392628191',
            'appSecret' => '207526e03349772ff9d279761ba73e75',
            'scope' => ['user_friends']
        ],

        // 'social' => [
        //     'class' => 'app\components\Social',
        //     'vk' => [
        //         'appId' => '4805460',
        //         'apiSecret' => 'hJcmJxnhgyY0PD7cNgy7',
        //     ],
        //     'fb' => [
        //         'appId' => '1576187392628191',
        //         'apiSecret' => '207526e03349772ff9d279761ba73e75',
        //     ]
        // ]
        // 
        // 'cache' => [
        //     'class' => 'yii\caching\MemCache',
        //     'servers' => [
        //         [
        //             'host' => 'localhost',
        //             'port' => 11211,
        //             'weight' => 16,
        //         ],
        //     ],
        // ],

        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'K3frgRc2kJAkKGLzp0dXgogZ9JZnLzUe',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
                    'enablePrettyUrl' => true,
                    'showScriptName' => false,
                    'enableStrictParsing' => true,
                    'rules' => [
                        'login'=>'site/login',
                        '/' =>'site/index',
                        
                        'users' => 'users/index',
                        'users/<id:\d+>' => 'users/view',

                        'users/watchlist' => 'users/watchlist',
                        'users/friends' => 'users/friends',
                        
                        'users/<hunter_id:\d+>/ban' => 'users/ban',
                        'users/<hunter_id:\d+>/unban' => 'users/unban',
                        'users/<victim_id:\d+>/like' => 'users/like',
                        'users/<victim_id:\d+>/dislike' => 'users/dislike',

                        'login/<social_name:(fb|vk)>' => 'site/login',

                        

                        // 'invite/<social_name:(vk|fb)>/<social_id:[\d]+>'=>'users/invite',

                        // 'social/friends'=>'social/friends',
                        // 'social/<social_id:[\d]+>/invite'=>'social/invite',
                        // 
                        
                        'social/invite'=>'social/invite',

                        
                        'auth/<social_name:(fb|vk|)>' => 'site/auth',
                        'logout'=> 'site/logout',
                        'profile'=>'users/profile',
                        'profile/update'=>'users/update', 
                    ],
                ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'=>'yii\gii\Module',
        'allowedIPs'=>['*']
        ];
}

return $config;
