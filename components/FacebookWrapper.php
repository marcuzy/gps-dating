<?php

namespace app\components;

use yii\base\Component;
use Yii;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;
use yii\helpers\Url;

class FacebookWrapper extends Component 
{
	public $appId = '';
	public $appSecret = '';
	public $session = null;
	public $scope = '';

	public function init() 
	{
		parent::init();
		FacebookSession::setDefaultApplication($this->appId, $this->appSecret);

		if (!Yii::$app->user->isGuest) {
			$self = Yii::$app->user->getIdentity();

			if ($self->isFb) {
				$this->session = new FacebookSession($self->access_token);
			}
		}
	}

	public function getLoginUrl() 
	{
		Yii::$app->session->open();
		$helper = new FacebookRedirectLoginHelper( Url::to(['auth/fb'], 'http') );
		return $helper->getLoginUrl($this->scope);
	}

	public function api($method, $params = [])
	{
		return (new FacebookRequest(
		                             $this->session, 'GET', $method, $params
		                           ))->execute();
	}

	public function createSession() 
	{
		$helper = new FacebookRedirectLoginHelper( Url::to(['auth/fb'], 'http')  );
		$session = null;
		try {
		  $session = $helper->getSessionFromRedirect();
		} catch(FacebookRequestException $ex) {
		  // When Facebook returns an error
			return null;
		} catch(\Exception $ex) {
		  // When validation fails or other local issues
			return null;
		}
		if ($session) {
		  // Logged in
			$this->session = $session;
			return $session;
		} else {
			return null;
		}
	}

	public function getMyFriends() 
	{

	}

	public function genderToInt($genderName) 
	{
		switch ($genderName) {
			case 'male':
				return 1;
			case 'female':
				return 2;
			default:
				return 0;
		}
	}

}