<?php

namespace app\components;

use yii\web\Controller;
use Yii;
use yii\web\Response;

class DatingController extends Controller
{
	protected function getSelf() 
    {
        return Yii::$app->user->getIdentity();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if (YII_DEBUG && Yii::$app->request->get('responseType', null))
            {
                $respString = Yii::$app->request->get('responseType', null);
                
                switch ($respString) {
                    case 'json':
                        $resp = Response::FORMAT_JSON;
                        break;
                    
                    default:
                        $resp = Response::FORMAT_HTML;
                        break;
                }

                Yii::$app->response->format = $resp;

            } else {
                Yii::$app->response->format = $this->defaultResponseFormat();
                $responseFormats = $this->responseFormats();
                foreach ($responseFormats as $format => $actions) {
                    if (in_array($action->id, $actions)) {
                        Yii::$app->response->format = $format;
                        break;
                    }
                }  
            }
                
            $this->setGPS();

            return true;
        } else {
            return false;
        }
    }

    /**
     * Были ли в запросе переданы координаты
     * @var boolean
     */
    private $requestHasCoords = false;
    /**
     * Если переданы координаты - сохраним в базу
     */
    protected function setGPS()
    {
        $lat = Yii::$app->request->get('lat', null);
        $lon = Yii::$app->request->get('lon', null);
        $ac = Yii::$app->request->get('ac', null);

        if (!Yii::$app->user->isGuest){

            $this->requestHasCoords = !is_null($lat) || !is_null($lon) || !is_null($ac);

            if (!is_null($lat))
                $this->self->lat = $lat;

            if (!is_null($lon))                   
                $this->self->lon = $lon;
            
            if (!is_null($ac))
                $this->self->ac = $ac;

            if ($this->requestHasCoords)
                $this->self->update_time = time();
        }
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        //если модель пользователя не была сохранена (а координаты обновляются с каждым запросом)
        if (!Yii::$app->user->isGuest && $this->requestHasCoords && !$this->self->isModelSaved)
            $this->self->save();
            
        return $result;
    }

    public function responseFormats() 
    {
    	/**
    	 * [
    	 * 	Response::FORMAT_JSON => ['index', 'phones', ..],
    	 * 	...
    	 * ]
    	 */
    	return [
    	];
    }

    public function defaultResponseFormat()
    {
    	return Response::FORMAT_JSON;
    }

}