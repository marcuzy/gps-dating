<?php

namespace app\components;

use yii\grid\Column;
use yii\helpers\Html;


class VkUrlColumn extends Column
{

    public $name = '';

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $url = "https://vk.com/id{$key}";
        return Html::a($model->name, $url, ['target'=>'_blank']);
        // $pagination = $this->grid->dataProvider->getPagination();
        // if ($pagination !== false) {
        //     return $pagination->getOffset() + $index + 1;
        // } else {
        //     return $index + 1;
        // }
    }

    protected function renderHeaderCellContent(){
        return "Url";
    }
}