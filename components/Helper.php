<?php

namespace app\components;

class Helper{

    static function isEmptyToFill($from, $to) {
        return empty($from) && !empty($to);
    }

    static function isfillToEmpty($from, $to) {
        return !empty($from) && empty($to);
    }

}
