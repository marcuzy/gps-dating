<?php

namespace app\components;

use yii\base\Component;
use Yii;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use yii\helpers\Url;
use app\components\Vk\Vk;
use app\models\User;

class Social extends Component 
{
	public $appId = '';
	public $appSecret = '';
	public $accessToken = '';
	public $vk = null;
	public $fb = null;
	public $self = null;
	
	private $_socialType = -1;

	public $data = [];

	public $id = 0;
	public $lastName = '';
	public $firstName = '';
	public $bDate = 0;
	public $gender = 0;
	public $status = '';
	public $avatar100 = '';
	public $avatar200 = '';

	public function init() 
	{
		parent::init();
	
		if (!$this->isGuest) {
			$this->self = Yii::$app->user->getIdentity();
			$this->accessToken = $this->self->access_token;
			$this->socialType = $this->self->social_type;
			$this->initApi();
			$this->initSession();
		}
	}

	public function getSocialType()
	{
		return $this->_socialType;
	}

	public function setSocialType($type)
	{
		if ($type === User::SOCIAL_VK || $type !== User::SOCIAL_FB)
			$this->_socialType = $type;
		else
			throw new Exception('Incorrect social type');
	}

	public function getIsVk()
	{
		return $this->socialType === User::SOCIAL_VK;
	}

	public function getIsFb()
	{
		return $this->socialType === User::SOCIAL_FB;
	}

	protected function initApi()
	{
		if ($this->isFb) {
			FacebookSession::setDefaultApplication($this->appId, $this->appSecret);
		} else if ($this->isVk) {
			$this->vk = new Vk;
			$this->vk->app_id = $this->appId;
			$this->vk->api_secret = $this->appSecret;
		} else
			throw new Exception('Incorrect social type.')
	}

	protected function initSession()
	{
		if ($this->isFb) {
			$this->fb = new FacebookSession($this->accessToken);
		} else {
            $this->vk->access_token = $this->self->accessToken;
		}
	}

	protected function getIsGuest()
	{
		return Yii::$app->user->isGuest;
	}

	public function login($code, $socialType)
	{
		$this->socialType = $socialType;
		$this->createSession();
		$this->loadProfileData();

	    $user = User::findOne(['social_id' => $this->id ]);

        if (!$user) {

            $user = new User;
            $user->social_id = $this->id;
            $user->social_type = $socialType;
            
            $user->load(['User'=>[
            	'last_name' => $this->lastName,
            	'first_name' => $this->firstName,
            	'bdate' => $this->bDate,
            	'avatar_100' => $this->avatar100,
            	'avatar_200' => $this->avatar200,
            	'gender' => $this->avatar200,
            	'status' => $this->status
            	]]);
        } 
	        
        $user->access_token = $this->accessToken;
        
        if ($user->validate()) {
            $isNewRecord = $user->isNewRecord;

            if ($user->save()) {

                //Перебор всех инвайтов для этого пользователя
                //и награждение каждого прикласившего
                if ($isNewRecord) {
                    $invites = Invite::find()->where(['social_id' => $this->id, 'social_type' => $user->social_type])->all();
                    // $command = Yii::$app   ->db
                    //             ->createCommand("UPDATE `{User::tableName()}` as `u`
                    //                                 SET `rating` = `rating`+{User::RATING_INVITE_FRIEND}
                    //                                 WHERE `id` IN (SELECT `user_id` FROM `{Invite::tableName()}` as `i` 
                    //                                                 WHERE i.social_id = {$this->id})");
                    // $command->execute();

                    if ($invites)
                        foreach ($invites as $invite) {   
                            $invite->owner->addFriend($user->id);
                            $invite->owner->rate(User::RATING_INVITE_FRIEND);
                            $invite->owner->save();
                            $invite->delete();    
                        }
                    
                }
            } else {
            	throw new Exception("Save is failed");
            }  

            return Yii::$app->user->login($user, 3600*24*30);

        } else
        	throw new Exception("Validate is failed");
	}

	protected function createSession() 
	{
		if ($this->socialType === User::SOCIAL_FB) {
			$helper = new FacebookRedirectLoginHelper( Url::to(['auth/fb'], 'http')  );
			$session = null;
			try {
			  $session = $helper->getSessionFromRedirect();
			} catch(FacebookRequestException $ex) {
			  // When Facebook returns an error
				return null;
			} catch(\Exception $ex) {
			  // When validation fails or other local issues
				return null;
			}
			if ($session) {
			  // Logged in
				$this->fb = $session;
				return $session;
			} else {
				return null;
			}
		} else {
			$response = $this->vk->getAccessToken($code, Url::to(['auth/vk'], 'http'));
            $this->id = $response['user_id'];
		}	
	}

	protected function api($method, $params)
	{
		return $this->isFb ? $this->apiFb($method, $params) : $this->apiVk($method, $params);
	}

	protected function apiFb($path, $params, $httpMethod='GET')
	{
		return (new FacebookRequest(
          $this->fb, $httpMethod, $path, $params
        ))->execute();
	}

	protected function apiVk($method, $params, $httpMethod='GET')
	{
		return $this->vk->api($method, $params, 'array', $httpMethod);
	}

	public function loadProfileData()
	{
		if ($this->isVk)
		{
			
		}
	}
}