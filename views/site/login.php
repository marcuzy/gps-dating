<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= Html::a('Login with VK', $vkAuthUrl) ?>
<?= Html::a('Login with Facebook', $fbAuthUrl) ?>

<?php if (\Yii::$app->session->hasFlash('forbidden')) : ?>
	<p class="flash bg-danger">
		Ваш профиль ВКонтакте не был приглашен к доступу на этот сайт.
	</p>

<?php endif; ?>