module.exports = function ( grunt ) {

	var globalConfig = {
		platform: '',
		defaultFiles: [
		    'libs/jquery.js',
		    'libs/json2.js',
		    'libs/underscore.js',
		    'libs/jquery-ui.js',
		    'libs/backbone.js',
		    'libs/idangerous.swiper-2.1.min.js',
		    'libs/perfect-scrollbar.with-mousewheel.min.js',
		    'src/translate.js',
		    'src/helpers.js',
		    'src/tbapp.js',
		    'src/models.js',
		    'src/modal.js',
		    'src/server.js',
		    'src/login.js',
		    'src/welcome.js',
		    'src/watchlist.js',
		    'src/friends.js',
		    'src/profile.js',
		    'src/users.js',
		    'src/header.js',
		    'src/main.js',
		    'src/controller.js',
		    'src/resize.js',		    
		]
	};

	// Project configuration.
	grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),
	globalConfig: globalConfig,
	jshint: {
		options: {
			jshintrc: '.jshintrc',
			reporter: require('jshint-stylish')
		},
		all: [
			'libs/*.js', 'src/*.js'
		]
	},

	cssmin: {
		combine: {
            options: {
                keepSpecialComments: 0
            },
			files: {
				'player.min.css': ['css/*.css']
			}
		}
	},

	clean: {
		build: ['player*.js']
	},

	concat: {
		options: {
			platform: ''
		},
		all: {
			src: globalConfig.defaultFiles,
			dest: 'player.js'
		}
	},

	uglify: {
		build: {
			files: {
				'player.min.js': 'player.js'
			}
		}
	},

	watch: {
		all: {
			files: globalConfig.defaultFiles.concat([ 'Gruntfile.js', 'css/*.css' ]),
			tasks: ['build']
		}
	}
	});

	require('load-grunt-tasks')(grunt);

	grunt.registerTask('default', ['clean']);
	grunt.registerTask('build', 'Build Telebreeze Player', function ( target ) {

		grunt.task.run('clean', 'concat:all','uglify', 'cssmin');
	});
};