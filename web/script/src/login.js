var loginView = null;
(function($){  
  var loginTimer = null;
  var LoginView = Backbone.View.extend({
    el: $('body'),    
    events: {
      'submit': 'onSubmit'
    },
    initialize: function(){
       _.bindAll(this, 'render','submit','onSubmit','show');              
    },   
    setButtonText: function(text, timeout)
    {
        if (this.loginTimer) clearTimeout(this.loginTimer);
        
        $('#login-auth-button').val(translator.t(text));
        $('#login-auth-text').html(translator.t(text));
        var tmp = function() {
            $('#login-auth-button').val(translator.t("Sign In"));                
            $('#login-auth-text').html('');
        };        
        if (timeout > 0)
            this.loginTimer = setTimeout(tmp, timeout);
    },
    onSubmit: function() {       
        
        if (tbapp.screen != 'login')
            return;
        
        if (this.loginTimer) clearTimeout(this.loginTimer);
        
        tbapp.user.login = $('#login-account').val();
        if ($('#login-password').val())
            tbapp.user.password = $.MD5($('#login-password').val());
        else
            tbapp.user.password = '';
        
        if (tbapp.user.login.indexOf(" ") >= 0)
            tbapp.user.login = '';
        
        if(tbapp.user.login == '' || tbapp.user.password == '')
        {           
            this.setButtonText("Enter login and password...", 5000);                        
        }
        else        
          this.submit();  
        
        return false;
    },
    submit: function(){              
        
        if(getUriParam(location.href, 'auth') == 'complete')                     
            Server.updateData(true);        
        else
            this.show();
        
        return false;
    },
    
    show: function() {
        $(".input_block").show();
        $("#loading-indicator").remove();
        $("#welcome_at_indicator").remove();
        initScroll($(".authorization_main"));
    },
    
    render: function(){
        
     tbapp.screen = 'login';
                        
     var self = this;
     var html = '<div class="authorization_main">\
            <div class="input_block" style="display: none">\
                    <span class="input_link_btn color2"></span><div class="input_link t">Sign In</div>\
                    <div class="input_main">\
                            <form>';                                                                                                                          

                            html += '<div class="social_network">';
                            html += '<p class="social_p bold t">Авторизируйтесь через социальную сеть</p>';
                            html += '<div class="icons">';

                            //var vk_url = 'https://oauth.vk.com/authorize?client_id=4805460&scope=friends&redirect_uri=http%3A%2F%2Fsearch-fb2.ddns.net%2Fauth%2Fvk&response_type=code';
                            //var fb_url = 'https://www.facebook.com/v2.2/dialog/oauth?client_id=1576187392628191&redirect_uri=http%3A%2F%2Fsearch-fb2.ddns.net%2Fauth%2Ffb&state=a8aa90e06079c18722cd7a544f675bb0&sdk=php-sdk-4.0.20&scope=user_friends';
                            html += '<a href="/login/vk" class="selectable_frame vkontakte social_btn"></a>';                                
                            html += '<a href="/login/fb" class="selectable_frame facebook social_btn"></a>';                            
                            html += '</div>';
                            html += '</div>';
                            
                            html += '<div id="login-auth-text" class="welcome_p"></div>';
                    
                            html += '</form>\
                    </div>\
                </div>';
                        
                html += '<div id="loading-indicator"></div>';
                
        
            html += '</div>';
        if (tbapp.user && tbapp.user.id)
        {           
          html = '<div class="profile_block"><div class="profile_left"> \
                        <div class="profile_info">\
                            <img src="'+tbapp.user.avatar_big+'" class="avatar" />\
                            <p class="name" style="color: #222">'+tbapp.user.firstname+' ' + tbapp.user.lastname +'</p>'                                        
                            
                            html += '<div class="btns">';                                                               
                            
                            html += '<input type="text" onchange="tbapp.user.status = $(\'#input-status\').val(); Server.updateProfile();" id="input-status" placeholder="'+translator.t('What in your mind?')+'" autocomplete="off" value="'+tbapp.user.status+'"/>';
                           /* html += '<input type="text" onchange="tbapp.user.age = $(\'#input-age\').val(); Server.updateProfile();" id="input-age" placeholder="'+translator.t('Your age?')+'" value="'+tbapp.user.age+'"/>';
                            
                            html += '<select id="input-gender" onchange="tbapp.user.gender = $(\'#input-gender\').val(); Server.updateProfile();">';
                            
                            var sel = '';
                            if (tbapp.user.gender == 0)
                                sel = 'selected';
                                
                            html += '<option value="" '+sel+'> - '+translator.t('Your Gender')+' - </option>';                          
                            
                            if (tbapp.user.gender == 2)
                                sel = 'selected';
                            else
                                sel = '';
                            
                            html += '<option value="2" '+sel+'>'+translator.t('Male')+'</option>';

                            if (tbapp.user.gender == 1)
                                sel = 'selected';
                            else
                               sel = '';
                            
                            html += '<option value="1" '+sel+'>'+translator.t('Female')+'</option>';
                            
                            
                            html += '</select>'                          */
                            
                            html += '<div class="btn-cta invite color2 t invite_firends_from_profile" style="width: 100%">Пригласить друзей</div>';                                                                                   

                            html += '<div class="btn signout color2 t">Sign Out</div>';                                                       
                            
                            html += '</div>';
                            
                        html += '</div></div></div>';                                                                                                       
        }
        

        $("#main-container").html(html);

        $('.invite_firends_from_profile').on('click', function(event, originEvent, $prevElement){
            inviteFirends();
        });
      
       
        initScroll($(".profile_block"));
     
        if (tbapp.view == 'mobile')
            $(".input_block").addClass('mobile');
        
        //$$nav.current($(".facebook"));
      
       $('.profile_block .signout').on('click', function(event, originEvent, $prevElement){
           location.href = tbapp.host + "logout";
       });       
       
       $('.input_main .reg_a').on('click', function(event, originEvent, $prevElement){
           controller.register();           
       });
       
       $('.profile_block .packages').on('click', function(event, originEvent, $prevElement){
           controller.packages();           
       });
       
       $('.social_btn').on('click', function(event, originEvent, $prevElement){
           window.location.href = $(this).attr('data-url');
       });

       
       
       if (tbapp.user && tbapp.user.id)
       {
            console.log('rend');
            headerView.showBackBtn(true);
            headerView.render();            
       }
       else if (tbapp.was_signout == false)           
          this.submit();
        
       //$$nav.on();  
       
       translator.translate();

       sizer();
    }
  });  
  loginView = new LoginView();
})(jQuery);

