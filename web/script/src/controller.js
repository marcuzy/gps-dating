var controller = null;



(function () {

  window.App = {

    initialize: function () {                                                                                                                                                        
        tbapp = new Object();       
        window.tbapp = new TBApp('browser','desktop'); 
        tbapp.user_token = '';

        updateMyPosition();
        setInterval(updateMyPosition, 1000);



        var Controller = Backbone.Router.extend({
            routes: {
                "": "start", 
                "/": "start",              
                "_=_": "start"
            },
            
            clean: function() {
                $("#main-container").html('');
            },
            
              
            watchlist: function() {
                tbapp.screen = 'watchlist';
                Server.updateWatchList();
                this.clean();
                mainView.render();  
                watchUserListView.render(true);                                                      
                translator.translate();
            },

            friends: function() {
                tbapp.screen = 'friends';
                Server.updateFriendList();
                this.clean();
                mainView.render();  
                friendListView.render(true);                                                      
                translator.translate();
            },

            main: function() {
                tbapp.screen = 'users';
                this.clean();
                mainView.render();  
                userListView.render();                                                      
                translator.translate();
            },

            welcome: function() {
                tbapp.screen = 'welcome';
                this.clean();
                mainView.render();  
                welcomeView.render();                                                      
                translator.translate();
            },

            start: function () {                                                  
                
                this.clean();
                loginView.render();                                                  
                translator.translate();
            }                                    
        });

        controller = new Controller(); // Создаём контроллер

        Backbone.history.start();  // Запускаем HTML5 History push  
    }
    
  };
  
  $(document).ready(function() {
        App.initialize();
  });


})();

