var userListView = null;

(function($){    
     
  var UserView = Backbone.View.extend({
    tagName: 'div', // name of (orphan) root tag in this.el      
    scroll: null,
    events: {
        'click': 'select'
    },
    
    initialize: function(){
      _.bindAll(this, 'html');              
    },
    

    html: function() {                                      
        var el = this.model;

        console.log('udate user item ' + el.get('update_time'));
        
        var html = '';        
        html += '<div class="item channel_item selected_color3 hover_color nav-item nav-mouse-click" data-id="'+el.get('id')+'">';        
        html += '<img class="channel_logo" data-item="' + el.get('id') + '" src="'+el.get('avatar')+'" />';         
        html += '<div class="description">';
        
        html += '<p class="channel_name_p"><span class="channel_name epg_channel_name">' + el.get('firstname') + ' ' + el.get('lastname') + '</span></p>';

        if (el.get('status'))
            html += '<p class="show_name">' + el.get('status') + '</p>';

        html += '<p class="time">'+
        getDistance(tbapp.lat, tbapp.lon, el.get('lat'), el.get('lon')) + ' - ' +
        '<span class="timeago" data-time="' + el.get('update_time') + '">' + timeAgo(tbapp.servertime - el.get('update_time')) + '</span>';  

        if (el.get('real') == 1)
            html += ', <span style="color: #005500">реальный</span>';

        html += '</p>';                    


        html += '</div>';
        
        html += '</div>'; 

        return html;
    }
  });
  
  var UserListView = Backbone.View.extend({
    el: $('#main-container'),    
    currentProfileOnDisplay: 0,
    timer: null,
        
    initialize: function(){
       _.bindAll(this, 'render', 'refresh', 'selLastUser', 'showProfile', 'getListHtml', 'updateTimeAgo');             
       this.collection = userList;
       this.collection.bind('refresh', this.refresh);
       setInterval(this.updateTimeAgo, 1000);
       function updateServerData() {
            if (tbapp.screen == 'users') {
                Server.updateData(false);
                setTimeout(updateServerData, 300000);
            }
            else
                setTimeout(updateServerData, 5000);
       }
       setTimeout(updateServerData, 300000);  
    },       

    updateTimeAgo: function() {
        $(".timeago").each(function(){
            var t = $(this).attr('data-time');
            var now = parseInt($.now() / 1000);
            $(this).html(timeAgo(now - t + tbapp.servertime_delta));
        });
    },

    refresh: function()
    {                
        this.render();
    },     

    selLastUser: function(do_not_show_profile) {                
        
        if (typeof(do_not_show_profile) == 'undefined')
            do_not_show_profile = false;
        
        if ($("#left_block").is(":visible"))
        {                        
            var domEl = $("#left_block #channel-item-" + tbapp.sel_user_id+":visible");                        
            if (typeof(domEl) == "undefined" || !domEl.is(":visible")) {            
                domEl = $("#left_block .item:first");       
            }
            
            if (domEl && domEl.is(":visible")) {
                if (tbapp.view != 'mobile' && do_not_show_profile == false) {                    
                    this.showProfile(domEl.attr('data-id'));                    
                }
            }
            scrollToFocus();
        }
    },        
    
    showProfile: function (id) {        
        this.currentProfileOnDisplay = id;            
        currentUser = userList.get(id);
        if (currentUser)        
             userProfileView.render(); 

        headerView.updateTitle();       
    },
    
    getListHtml: function() {        
        var contentHtml = '';
        for(var i = 0; i < this.collection.length; i++)
        {       
            var el = this.collection.models[i];                        
            var userView = new UserView({model: this.collection.models[i]});              
            contentHtml += userView.html();                           
        }         
                

        return contentHtml;
    },
    
    render: function(){                                 


        
               
        var self = this;
           
        var contentHtml = this.getListHtml();
        if (contentHtml == '')
        {
            contentHtml = '<div style="font-size: 16px; line-height: 20px; font-weight: bold; margin-top: 20px; margin-bottom: 10px;">В данный момент рядом никого нет. Не паникуйте, просто пригласите друзей из Вашего города или зайдите позже.</div>';
            contentHtml += '<div class="btn-cta invite_firends_from_users">'+translator.t('Пригласить друзей')+'</div>';
        }
        
          var html = '<div class="left" id="left_block">';
                html += '<div class="contentHolder scrollable" style="padding-top: 15px;">';
                    html += '<div class="content">' + contentHtml + '</div>';  
                    html += '<div class="placeholder50"></div>';
                html += '</div>';
            html += '</div>';                                   
            html += '<div class="right" id="right_block">';                
                    html += '<div class="content"></div>';                
            html += '</div>';
   
        $('.main_scroll_block').html(html);

        
        headerView.showBackBtn(false);                                                           
                    
        $(".channel_item").on('click', function(){                                    
            $(".focus").removeClass("focus");
            $(this).addClass("focus");
            
            var id = $(this).attr('data-id');                                                       
            self.showProfile(id);             
            tbapp.sel_user_id = id;                        
            if (tbapp.view == 'mobile')
            {
                $("#left_block").hide();            
                $("#right_block").show();                 
                headerView.showBackBtn(true);             
            }
        });                          

        $('.invite_firends_from_users').on('click', function(event, originEvent, $prevElement){
            inviteFirends();
        });
        
        if (tbapp.view == 'mobile')
        {
            $("#left_block").css("width", "100%");
            $("#right_block").css("width", "100%");
            $("#right_block").hide();            
        }
              
              
        if (this.timer == null){
            this.timer = setInterval(this.updateProgress, 30000);
        }        
        
        this.scroll = initScroll($('#left_block .scrollable'));       

        
        sizer();
   
        this.selLastUser(); 

        document.title = "channels";                
    }   
  });  
  userListView = new UserListView();
})(jQuery);

