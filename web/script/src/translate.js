var msg = {      
    'ru': {
        'Join Me': 'Присоединяйся',     
        'Male': 'Мужской',
        'Sign In': 'Вход',
        'Female': 'Женский',
        'Your Gender': 'Ваш пол',
        'Sign Out': 'Выход',
        'Your age?': 'Ваш возраст?',
        'What in your mind?': 'Как Вас узнать?'
    }        
}


var Translator = Backbone.Model.extend({
  defaults: {language: 'ru'},
  t: function(text) {   
      this.language = 'ru';   
      if (typeof(text) != 'string' || (this.language != 'ru' && this.language != 'gr' && this.language != 'fr'))
          return text;
      
     
      var translated = msg[this.language][text];
      if (!translated)
          return text;            
            
      return translated;
  },
  translate: function() {
      var self = this;
      $(".t").each(function() {
          $(this).html(self.t($(this).html()));
      });
  }
});

translator = new Translator();





