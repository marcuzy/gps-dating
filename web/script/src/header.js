var headerView = null;
(function($){          
  var HeaderView = Backbone.View.extend({
    el: $('#main-container'), 
    logoUrl: '',
    showBackBtnFlag: false,
    initialize: function(){        
        _.bindAll(this, 'render','unrender','back');               
    },      
    setLogoUrl: function(url){        
        this.logoUrl = url;
        this.render();
    },
    showBackBtn: function(show){
        this.showBackBtnFlag = show;                                       
       
        if (show)
        {
            $(".header .back_btn").show();
            $(".search_wrapper").hide();
            tbapp.os.set('canFinish', '0');
        }
        else
        {
            $(".header .back_btn").hide();
            $(".search_wrapper").show();
            tbapp.os.set('canFinish', '1');
        }
    },
    showSearchBtn: function(show){        
       
        if (show)
        {          
            $(".search_wrapper").show();           
        }
        else
        {
            $(".search_wrapper").hide();                       
        }
    },
    back: function() {
        if (tbapp.screen == 'packages'){
            controller.start();
        }else if (tbapp.screen == 'register') {
            if (SB.platformName == 'android' || SB.platformName == 'ios') {
                controller.start();
                return false;        
            }
        }
        else if ($(".header .back_btn").is(":visible"))
        {
            if (tbapp.screen == 'vod')
                tbapp.screen = 'vodlist';

            controller.main();
            return false;        
        }
         
       return true;
        
    },

    updateTitle: function() {
        var dialog_name = '';
        if (tbapp.screen == 'users')
            dialog_name = 'Люди рядом с Вами';
        else if (tbapp.screen == 'friends')
            dialog_name = 'Друзья';
        else if (tbapp.screen == 'login')
            dialog_name = 'Ваш профиль';
        else if (tbapp.screen == 'watchlist')
            dialog_name = 'Кто смотрел мой профиль';
        else if (tbapp.screen == 'welcome')
            dialog_name = 'Секретный вопрос';
        else if (tbapp.screen == 'profile')
            dialog_name = ''

        $('.header .dialog_name').html(dialog_name);    
    },

    render: function(){    
               
        var self = this;
        $('.header').remove();
        
       var html = '<div class="header color1">';
       if (this.showBackBtnFlag == true)
       {
            tbapp.os.set('canFinish', '0');
            html += '<div class="back_btn selected_color3 nav-item"></div>';
        }
        else
        {
            tbapp.os.set('canFinish', '1');
            html += '<div class="back_btn selected_color3 nav-item" style="display: none"></div>';
            
            /*if ()
            {
                html += '<div class="search_wrapper">\
                        <input type="text" id="search-input" placeholder="'+translator.t('Search for...')+'"></input>\
                        <div class="search_icon color1"></div>\
                        <div class="clear"></div>\
                    </div>';
            }*/                
        }
                
 
        html += '<div class="dialog_name"></div>';
        
       
        html += '<div class="right_icons main_logo">\
        <div class="a_wrap main header-right">';
        

        if (tbapp.screen == 'watchlist')
            html += '<div class="btn settings watch_list" style="background-color: #555"></div>';
        else
            html += '<div class="btn settings watch_list"></div>';

        if (tbapp.screen == 'friends')
            html += '<div class="btn friends" style="background-color: #555"></div>';
        else
            html += '<div class="btn friends"></div>';

        if (tbapp.screen == 'login')
            html += '<div class="btn account" style="background-color: #555"></div>';
        else
            html += '<div class="btn account"></div>';

        html += '</div></div>';

        html += '</div>';

         $("#main-container").prepend(html);        
                                  
        $(".header .btn.settings").addClass('mobile');            
        $(".header .btn.account").addClass('mobile');
        $(".header .btn.news").addClass('mobile');
        $(".header .back_btn").addClass('mobile');
        $(".header .search_wrapper").addClass('mobile');
                                    
          
        $('.header .hidden').on('click', function(event, originEvent, $prevElement){
             $(this).toggleClass('open');
             $(".header .search_wrapper").removeClass("active");
             if ($(this).hasClass('open'))
             {
                 $(".header .logo").hide();
                 $(".header .a_wrap").show();
                 $(".search_wrapper").hide();
             }
             else
             {
                 $(".header .logo").show();                 
                 $(".header .a_wrap").hide();
                 $(".search_wrapper").show();
             }
             
        });

        $('.header .watch_list').on('click', function(event, originEvent, $prevElement){            
            controller.watchlist();
        });

        $('.header .friends').on('click', function(event, originEvent, $prevElement){            
            controller.friends();
        });
        
        $(".header .search_icon").on('click', function() {
            $(".header .search_wrapper").toggleClass("active");
            
            if (tbapp.view == 'mobile')
            {
                $(".header .logo").toggle();
            }
            
            if (!$(".header .search_wrapper").hasClass("active"))
            {
                $("#search-input").val('');
                if (tbapp.screen == 'channels')
                    channelListView.render();
                else if (tbapp.screen == 'vodlist') 
                    vodListView.render();                    
            }
        });
        
        var prevSearch = '';
        
        $('#search-input').on('keyup', function(e){
            
            var s =  $("#search-input").val();
            if (prevSearch != s)
            {
                prevSearch = s;            
                if (tbapp.screen == 'channels')
                    channelListView.render();
                else if (tbapp.screen == 'vodlist') 
                    vodListView.render();                    
             }
        });
        
        
        $('.header .back_btn').on('click', function(event, originEvent, $prevElement){
            self.back();
        });
                
        $('.header .account').on('click', function(event, originEvent, $prevElement){
            controller.start();            
        });

        this.updateTitle();
    },    
    unrender: function(){
      $('.header').remove();
    }    
  });  
  headerView = new HeaderView();
})(jQuery);

