var welcomeView = null;
(function($){    
  
  var WelcomeView = Backbone.View.extend({
    el: $('#main-container'), 
    curChannel: null,        
    initialize: function(){
        _.bindAll(this, 'render');                
    },
        
    render: function(){          
        
        var self = this;   
        
        tbapp.screen = 'welcome';
                                               
        var html = '';    

        html = '<div style="font-size: 16px; line-height: 24px; font-weight: bold;margin-bottom: 10px; padding: 20px">';

        html += '<p>Вопрос на сегодня: ' + tbapp.user.ask+'</p>';
        html += '<p>Ответ: ' + tbapp.user.answer+'</p>';
        html += '<br/>Рекомендации: <br/>';
        html += '<span style="font-weight: normal">';

        html += 'Используйте сегодняшний секретный вопрос для знакомства. Если к Вам обращаются и Вы не против пообщаться, отчечайте как указано выше.<br><br>';
        if (tbapp.user.real != 1) {
            html += '<p>Получите статус <span style="font-weight: bold">"реальный"</span>, чтобы увеличить доверие к своему профилю. Для этого пригласите как минимум трех своих друзей.</p>';
        }

        html += '<br><p>Зарабатывайте рейтинг, чтобы отображаться выше других:</p>';

        html += '<ul style="list-style-type: square; margin-left: 30px;">';
        //if (tbapp.user.age == 0 || tbapp.user.gender == 0) {
        //    html += '<li>Заполните профиль</li>';
        //}
        html += '<li>Приглашайте больше друзей</li>';
        html += '<li>Получайте лайки от других пользователей</li>';
        html += '</ul>';

        html += '</span>';

        html += '</div>';
        html += '<div class="btn-cta invite">'+translator.t('Пригласить друзей')+'</div>';
        html += '<div class="btn-cta start_meet" style="background-color: #33aa33">'+translator.t('Начать знакомства')+'</div>';

        
        $("#main-container").html(html);

        $('.start_meet').on('click', function(event, originEvent, $prevElement){
           headerView.back();
       });

        $('.invite').on('click', function(event, originEvent, $prevElement){
            inviteFirends();
        });
        
        
        $('.homepage_programma .i').on('click', function(event, originEvent, $prevElement){
            location.href = currentUser.get('social_url');
        });    
 
        this.scroll = initScroll($('#main-container'));
        sizer();                                                

        headerView.render();
        headerView.showBackBtn(true);                                                           
    }
  });  
  welcomeView = new WelcomeView();
})(jQuery);

