var watchUserListView = null;

(function($){    
     
  var WatchUserView = Backbone.View.extend({
    tagName: 'div', // name of (orphan) root tag in this.el      
    scroll: null,
    events: {
        'click': 'select'
    },
    
    initialize: function(){
      _.bindAll(this, 'html');              
    },
    

    html: function() {                                      
        var el = this.model;

        console.log('udate user item ' + el.get('update_time'));
        
        var html = '';        
        html += '<div class="item channel_item selected_color3 hover_color nav-item nav-mouse-click" data-id="'+el.get('id')+'">';        
        html += '<img class="channel_logo" data-item="' + el.get('id') + '" src="'+el.get('avatar')+'" />';         
        html += '<div class="description">';
        
        html += '<p class="channel_name_p"><span class="channel_name epg_channel_name">' + el.get('firstname') + ' ' + el.get('lastname') + '</span></p>';    

        html += '<p class="time">'+'<span class="timeago" data-time="' + el.get('watch_time') + '">' + timeAgo(tbapp.servertime - el.get('watch_time')) + '</span>';  

        if (el.get('real') == 1)
            html += ', <span style="color: #005500">реальный</span>';

        html += '</p>';                    
        
        html += '</div>';

        if (el.get('liked') == '1')
            html += '<span class="liked"></span>';
        

        if (el.get('banned') == '1')
            html += '<span class="ban banned" data-user-id="'+el.get('id')+'"></span>';
        else
            html += '<span class="ban" data-user-id="'+el.get('id')+'"></span>';

        var social_class = 'vk_profile';
        if (el.get('social_type') == '2')
            social_class = 'fb_profile';

        html += '<div class="'+social_class+' social_btn" data-url="'+el.get('social_url')+'" style="right: 0; position: absolute;"></div>';

        
        html += '</div>'; 

        return html;
    }
  });
  
  var WatchUserListView = Backbone.View.extend({
    el: $('#main-container'),    
    currentProfileOnDisplay: 0,
    timer: null,
        
    initialize: function(){
       _.bindAll(this, 'render', 'refresh', 'selLastUser', 'showProfile', 'getListHtml', 'updateTimeAgo');             
       this.collection = watchUserList;
       this.collection.bind('refresh', this.refresh);
       setInterval(this.updateTimeAgo, 1000);       
    },       

    updateTimeAgo: function() {
        $(".timeago").each(function(){
            var t = $(this).attr('data-time');
            var now = parseInt($.now() / 1000);
            $(this).html(timeAgo(now - t + tbapp.servertime_delta));
        });
    },

    refresh: function()
    {                
        this.render();
    },     

    selLastUser: function(do_not_show_profile) {                
        
        if (typeof(do_not_show_profile) == 'undefined')
            do_not_show_profile = false;
        
        if ($("#left_block").is(":visible"))
        {                        
            var domEl = $("#left_block #channel-item-" + tbapp.sel_user_id+":visible");                        
            if (typeof(domEl) == "undefined" || !domEl.is(":visible")) {            
                domEl = $("#left_block .item:first");       
            }
            
            if (domEl && domEl.is(":visible")) {
                if (tbapp.view != 'mobile' && do_not_show_profile == false) {                    
                    this.showProfile(domEl.attr('data-id'));                    
                }
            }
            scrollToFocus();
        }
    },        
    
    showProfile: function (id) {        
        this.currentProfileOnDisplay = id;            
        var currentUser = userList.get(id);
        if (currentUser)        
             userProfileView.render();        
    },
    
    getListHtml: function() {        
        var contentHtml = '';
        for(var i = 0; i < this.collection.length; i++)
        {       
            var el = this.collection.models[i];                        
            var userView = new WatchUserView({model: this.collection.models[i]});              
            contentHtml += userView.html();                           
        }         
                

        return contentHtml;
    },
    
    render: function(show_loader){                                 
        
                    

        var self = this;
        
        var contentHtml = '';
        if (show_loader == true)   
            contentHtml = '<div id="loading-indicator"></div>';
        else
            contentHtml = this.getListHtml();

        if (contentHtml == '')
        {
            contentHtml = '<div style="font-size: 16px; line-height: 20px; font-weight: bold; margin-top: 20px; margin-bottom: 10px;">За последние 30 дней никто не смотрел Вашу анкету. Чтобы привлечь к себе внимание сделайте свой статус реальным и поднимите свой рейтинг, для этого необходимо всего навсего пригласить как минимум трех своих друзей в приложение.</div>';
            contentHtml += '<div class="btn-cta invite_from_watch">'+translator.t('Пригласить друзей')+'</div>';
        }
        
          var html = '<div class="left" id="left_block">';
                html += '<div class="contentHolder scrollable" style="padding-top: 15px;">';
                    html += '<div class="content">' + contentHtml + '</div>';  
                    html += '<div class="placeholder50"></div>';
                html += '</div>';
            html += '</div>';                                   
            html += '<div class="right" id="right_block">';                
                    html += '<div class="content"></div>';                
            html += '</div>';
   
        $('.main_scroll_block').html(html);

        $('.invite_from_watch').on('click', function(event, originEvent, $prevElement){
            inviteFirends();
        });
        
        headerView.showBackBtn(true);                                                                                                                    
                    
        $(".channel_item").on('click', function(){                                    
            $(".focus").removeClass("focus");
            $(this).addClass("focus");
            
            var id = $(this).attr('data-id');                                                       
            self.showProfile(id);             
            tbapp.sel_user_id = id;                        
            if (tbapp.view == 'mobile')
            {
                $("#left_block").hide();            
                $("#right_block").show();                 
                headerView.showBackBtn(true);             
            }
        });                          
        
        if (tbapp.view == 'mobile')
        {
            $("#left_block").css("width", "100%");
            $("#right_block").css("width", "100%");
            $("#right_block").hide();            
        }

        $('.social_btn').on('click', function(event, originEvent, $prevElement){
            location.href = $(this).attr('data-url');
            return false;
        });

        $('.ban').on('click', function(event, originEvent, $prevElement){
           var ban = true;
           if ($(this).hasClass('banned'))
                ban = false;
   
            if (ban) 
            {
                $(this).addClass('banned');            
            }
            else
            {
                $(this).removeClass('banned');                   
            }

           Server.ban(ban, $(this).attr('data-user-id'));
           return false;
        });
              
              
        if (this.timer == null){
            this.timer = setInterval(this.updateProgress, 30000);
        }        
        
        this.scroll = initScroll($('#left_block .scrollable'));       

        
        sizer();
   
        this.selLastUser(); 

        document.title = "channels";                
    }   
  });  
  watchUserListView = new WatchUserListView();
})(jQuery);

