var userProfileView = null;
(function($){    
  
  var UserProfileView = Backbone.View.extend({
    el: $('#right_block .content'), 
    curChannel: null,    
    scroll: null,
    initialize: function(){
        _.bindAll(this, 'render','refresh', 'fillTemplate', 'template', 'getCurChannel', 'updatePosition' ,'updateCompass'); 

        var self = this;
        function updateDialog(){
        	if ((tbapp.screen == 'profile' && tbapp.view == 'mobile') || (tbapp.screen == 'users' && tbapp.view != 'mobile'))
        		self.updatePosition();
        }        
        setInterval(updateDialog, 30000);

        var $this = this;
       
        window.addEventListener('deviceorientation', $this.updateCompass, false); 


           

    },
    refresh: function(){
        this.render();
    },
    getCurChannel: function () {        
        return this.curChannel;
    },
    setCurChannel: function(channel){        
        //if (!this.curChannel || channel.get('id') != this.curChannel.get('id'))
        {
            this.curChannel = channel;        
            $('#epg-list').html('<div id="small-loading-indicator"></div>');
            return true;
        }
        return false;
    },
    showInfo: function(){
        $('.homepage_programma #Default3').hide();
        $('.homepage_programma .channel_info').show();        
    },
    hideInfo: function(){
        $('.homepage_programma .channel_info').hide();        
        $('.homepage_programma #Default3').show();
    },
    scrollToLive: function() {
        var sel = $(".now");    
    
        if (typeof(sel) == 'undefined')
            return;

        var pos = sel.position();
        var list = sel.parents(".scrollable");    
        if (typeof(pos) != 'undefined' && typeof(list) != 'undefined') {        
            list.scrollTop(list.scrollTop() + pos.top - list.height() / 2 + sel.height() / 2);        
            list.perfectScrollbar('update');
        }   
    },
    
    clean: function() {
        if (this.scroll)
        {
           // this.scroll.perfectScrollbar('destroy');
            this.scroll = null;
        }
    },
    
    template: function()
    {
        
        var html = '<div class="homepage_programma">\
        <div class="programma_head">\
        <div class="ph_main">\
        <p class="channel_name"><span class="bold"></span></p>\
        <div class="like"></div>\
        <div class="fav map"></div>\
        <div class="i"></div>\
        </div>\
        </div>\
        <div id="Default3" class="contentHolder scrollable">\
        <div id="epg-list"></div>\
        <div class="placeholder50"></div>\
        </div>\
        <div class="channel_info" style="padding-top: 5px;">\
                <div class="channel_info_block contentHolder">\
                    <p></p>\
                    <div class="profile_avatar" style="    margin: 0 auto !important; display: block; padding-left: 20px; padding-right: 20px; margin: auto">\
                        <img src="" alt="" style="width: 100%;">\
                    </div>\
                    <div class="profile_details">\
                        <div class="profile_status"></div>\
                        <div class="profile_arrow"></div>\
                        <div class="profile_distance"></div>\
                        <div class="askanswer">Вопрос: ' + tbapp.user.ask +'<br>Ответ: '+tbapp.user.answer+'</div>\
                    </div>\
                </div>\
            </div>\
        </div>';
        
        return html;
    },

    updateMyPosition: function(callback) {
        callback = callback || function(){};
        navigator.geolocation.getCurrentPosition(function(position) {
                tbapp.lat = position.coords.latitude;
                tbapp.lon = position.coords.longitude;
                tbapp.ac =  position.coords.accuracy; 
                callback();                                 
         });
        setTimeout(this.updateMyPosition, 10000);
    },
    
    updatePosition: function() {
                
        var url = tbapp.host + "users/"+currentUser.get('id')+"?lat=" + tbapp.lat + '&lon=' + tbapp.lon + '&ac=' + tbapp.ac;            
        tbapp.os.makeRequest(
             url,
             "GET",
             true,
             'json',
             function(json) {    
                            
                
                tbapp.user.ask = json.ask;
                tbapp.user.answer = json.answer;
                $.each(json.users ,function(key, el){     
                    var lat = el.lat;
                    var lon = el.lon;  
                    currentUser.set('lat', lat);
                    currentUser.set('lon', lon);
                    var text = getDistance(tbapp.lat, tbapp.lon, lat, lon);
                    text += '<br><span class="timeago" data-time="' + el.update_time + '">' + timeAgo(tbapp.servertime - el.update_time) + '</span>';  

                    $(".homepage_programma .profile_distance").html(text);    
                    return false;
                                        
                });

                 
            },
             function() { Server.failResp(); }
         );                       

        var deg = parseInt(Math.random() * 45) - 120;
        
        $(".homepage_programma .profile_arrow").css('-ms-transform', 'rotate('+deg+'deg)');
        $(".homepage_programma .profile_arrow").css('-webkit-transform', 'rotate('+deg+'deg)');
        $(".homepage_programma .profile_arrow").css('transform', 'rotate('+deg+'deg)');                   
    },

    updateCompass: function(coords) {

        if (typeof currentUser === 'undefined' ||  currentUser===null) return;

        var meters = parseInt(getDistanceInMeters(tbapp.lat, tbapp.lon, currentUser.get('lat'), currentUser.get('lon')));
        console.log(meters);
        if (meters > 30)
        {
            var angleToAim = Math.atan( (currentUser.get('lat') - tbapp.lat) / (currentUser.get('lon') - tbapp.lon)  ) * 180;

            var text = getDistance(tbapp.lat, tbapp.lon, currentUser.get('lat'), currentUser.get('lon'));
            text += '<br/><span class="timeago" data-time="' + currentUser.get('update_time') + '">' + timeAgo(tbapp.servertime - currentUser.get('update_time')) + '</span>';  

            $(".homepage_programma .profile_distance").html(text);  
            var deg = (coords.alpha - angleToAim);
           
            $(".homepage_programma .profile_arrow").css('-ms-transform', 'rotate('+deg+'deg)');
            $(".homepage_programma .profile_arrow").css('-webkit-transform', 'rotate('+deg+'deg)');
            $(".homepage_programma .profile_arrow").css('transform', 'rotate('+deg+'deg)');       
            $(".homepage_programma .profile_arrow").show();
        }
        else
        {
            $(".homepage_programma .profile_distance").html('Вы совсем рядом');  
            //$(".homepage_programma .profile_arrow").hide();
        }        
    },
    
    fillTemplate: function() {                      
            
        var self = this;
        $('.epg_item').off('nav_focus');
        $('.homepage_programma .epg_item').off('click');
        
        $(".profile_avatar img").attr('src', currentUser.get('avatar_big'));

        var name = currentUser.get('firstname') + ' ' + currentUser.get('lastname');
        if (currentUser.get('age') > 0)
        	name += ', ' + currentUser.get('age');

        $(".homepage_programma .i").removeClass('fb_profile');
        $(".homepage_programma .i").removeClass('vk_profile');
        if (currentUser.get('social_type') == '1')
            $(".homepage_programma .i").addClass('vk_profile');
        else if (currentUser.get('social_type') == '2')
            $(".homepage_programma .i").addClass('fb_profile');


        $(".homepage_programma .channel_name span").html(name);   

        $(".homepage_programma .like").html(currentUser.get('likes'));   
        if (currentUser.get('liked') == '1')
            $(".homepage_programma .like").addClass('active');   

        $(".homepage_programma .profile_status").html(currentUser.get('status'));   
        $(".homepage_programma .profile_age").html('Age: <b>' + currentUser.get('age') + '</b>');   

        var text = getDistance(tbapp.lat, tbapp.lon, currentUser.get('lat'), currentUser.get('lon'));
        text += '<br/><span class="timeago" data-time="' + currentUser.get('update_time') + '">' + timeAgo(tbapp.servertime - currentUser.get('update_time')) + '</span>';  

        $(".homepage_programma .profile_distance").html(text);   
        
       
        $(".homepage_programma .i").show();
           
        $(".homepage_programma .map").addClass('enabled');   
                 
        $(".homepage_programma .channel_info").show();                
                
        $(".homepage_programma .channel_info .close").show();        
        
        $(".homepage_programma .channel_info_block p").html('');
        $(".homepage_programma .channel_info_block .online").attr('data-nav_ud', "0,0,0,#channel-item-"+currentUser.get('id'));                                 
        
        
                          
        this.showInfo();
        $('#epg-list').html('');                                                                  
    },    
        
    render: function(){          
        
        var self = this;   

        if (tbapp.view == 'mobile')
        	tbapp.screen = 'profile';
                               
        if ($(".homepage_programma").is(":visible"))
        {                           
            this.fillTemplate(); 
            sizer();
            return;
        }                
        
        $("#right_block .content").html(this.template());
        this.fillTemplate();

        this.updateCompass();
        
        $('.homepage_programma .i').on('click', function(event, originEvent, $prevElement){
            location.href = currentUser.get('social_url');
        });

        $('.homepage_programma .map').on('click', function(event, originEvent, $prevElement){                        
            location.href = 'http://www.google.com/maps/place/'+currentUser.get('lat')+','+currentUser.get('lon');
        });                                  

        $('.like').on('click', function(event, originEvent, $prevElement){
           var like = true;
           if ($(this).hasClass('active'))
                like = false;

            var v = parseInt($(this).html());
            if (like) 
            {
                $(this).addClass('active')
                $(this).html(v + 1);
            }
            else
            {
                $(this).removeClass('active')   
                $(this).html(v - 1);
            }

           Server.like(like, currentUser.get('id'));
        });
               
        $('.online').on('click', function(event, originEvent, $prevElement){
           
        });
                           
        $('.homepage_programma .channel_info .close').on('click', function(event, originEvent, $prevElement){
            self.hideInfo();
        });
        
 
        this.scroll = initScroll($('#right_block .scrollable'));
        sizer();                
        
        
        
        this.updatePosition();
        

    }
  });  
  userProfileView = new UserProfileView();
})(jQuery);

