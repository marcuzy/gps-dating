simulateTouchEvents = function(oo,bIgnoreChilds) {
    if( !$(oo)[0] )
    { return false; }
    if( !window.__touchTypes )
    {
        window.__touchTypes  = {touchstart:'mousedown',touchmove:'mousemove',touchend:'mouseup'};
        window.__touchInputs = {INPUT:1,TEXTAREA:1,SELECT:1,OPTION:1,'input':1,'textarea':1,'select':1,'option':1};
    }
    $(oo).bind('touchstart touchmove touchend', function(ev)
    {
        var bSame = (ev.target == this);
        if( bIgnoreChilds && !bSame )
        { return; }

        var b = (!bSame && ev.target.__ajqmeclk), // Get if object is already tested or input type
            e = ev.originalEvent;
        if( b === true || !e.touches || e.touches.length > 1 || !window.__touchTypes[e.type]  )
        { return; } //allow multi-touch gestures to work

        var oEv = ( !bSame && typeof b != 'boolean')?$(ev.target).data('events'):false,
            b = (!bSame)?(ev.target.__ajqmeclk = oEv?(oEv['click'] || oEv['mousedown'] || oEv['mouseup'] || oEv['mousemove']):false ):false;

        if( b || window.__touchInputs[ev.target.tagName] )
        { return; } //allow default clicks to work (and on inputs)

        // https://developer.mozilla.org/en/DOM/event.initMouseEvent for API
        var touch = e.changedTouches[0], newEvent = document.createEvent("MouseEvent");
        newEvent.initMouseEvent(window.__touchTypes[e.type], true, true, window, 1,
            touch.screenX, touch.screenY,
            touch.clientX, touch.clientY, false,
            false, false, false, 0, null);

        touch.target.dispatchEvent(newEvent);
        e.preventDefault();
        ev.stopImmediatePropagation();
        ev.stopPropagation();
        ev.preventDefault();
    });
    return true;
};


var iosCall = function (string){
            
    var iframe = document.createElement("IFRAME");
    todo = string;
    iframe.setAttribute("src",todo);
    document.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
    iframe = null;
    if (typeof(ios_result) != 'undefined')
    return ios_result;

}

var initScroll = function(el) {
                                        
    el.css('overflow-y', 'hidden');        
    el.css('overflow-x', 'hidden');
    el.css('-webkit-overflow-scrolling', 'touch');


    return el;    
}

function inviteFirends(){
    if (tbapp.user.social_type == 1)
        location.href = 'http://vk.com/share.php?url=http://cherrydating.me';
    else if (tbapp.user.social_type == 2)
        location.href = 'http://www.facebook.com/sharer.php?u=http://cherrydating.me&t=Я%20в%20CherryDating'; 
}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

function formatTime(date) {
    if (tbloader.getTimeFormat() == '12h')
        return formatAMPM(date);
    
    var hh = date.getHours();
    if ( hh < 10 ) hh = '0' + hh;

    var mm = date.getMinutes();
    if ( mm < 10 ) mm = '0' + mm;

    return hh + ':' + mm;
 }
 
 function monthToAbr(month)
 {
     if (month < 1 || month > 12)
         return '';
     
     var months = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
     return translator.t(months[month]);
 }
 
 var addParamToUrl = function(url, param, value)
 {
     if (url.indexOf("?") > 0)
         url += '&';
     else
         url += '?';
     
     url += param;
     url += '=';
     url += value;
     return url;
 }

var getUriParam = function(uri, strParam) {
    var res = "";
    var last = -1;
    var first = uri.indexOf(strParam + "=");
    if (first >= 0) {
        first += strParam.length + 1;
        last = uri.indexOf("&", first);
        if (last < 0)
            last = uri.length;
    }
    if (last > 0 && last != first) {
        res = uri.substring(first, last);
    }
    return res;
}

var removeParamFromUrl = function(url, param) {
    var value = getUriParam(url, param);
    if (value)
    {
        var par = param + '=' + value;
        var s = par + '&';
        if (url.indexOf(s) > 0)
            return url.replace(s, "");
        
        s = '&' + par;
        if (url.indexOf(s) > 0)
            return url.replace(s, "");
        
        s = '?' + par;
        if (url.indexOf(s) > 0)
            return url.replace(s, "");
    }
    return url;
}

var updateMyPosition = function(callback){
    callback = callback || function(){};
    navigator.geolocation.getCurrentPosition(function(position) {
            tbapp.lat = position.coords.latitude;
            tbapp.lon = position.coords.longitude;
            tbapp.ac =  position.coords.accuracy; 
            callback();                                 
     });
}

var getDistanceInMeters = function(lat1, long1, lat2, long2) {

    //радиус Земли
    var R = 6372795;
     
    //перевод коордитат в радианы
    lat1 *= Math.PI / 180;
    lat2 *= Math.PI / 180;
    long1 *= Math.PI / 180;
    long2 *= Math.PI / 180;
     
    //вычисление косинусов и синусов широт и разницы долгот
    var cl1 = Math.cos(lat1);
    var cl2 = Math.cos(lat2);
    var sl1 = Math.sin(lat1);
    var sl2 = Math.sin(lat2);
    var delta = long2 - long1;
    var cdelta = Math.cos(delta);
    var sdelta = Math.sin(delta);
     
    //вычисления длины большого круга
    var y = Math.sqrt(Math.pow(cl2 * sdelta, 2) + Math.pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
    var x = sl1 * sl2 + cl1 * cl2 * cdelta;
    var ad = Math.atan2(y, x);
    var dist = ad * R; //расстояние между двумя координатами в метрах

    return dist;
}


var timeAgo = function(sec) {
    if (sec <= 0)
        return 'только что';
    else if (sec < 60)
        return sec + ' сек. назад';
    else if (sec < 3600)
        return (parseInt(sec / 6) / 10) + ' мин. назад';
    else if (sec < 3600 * 24)
        return (parseInt(sec / 360) / 10) + ' час. назад';
    else 
        return (parseInt(sec / (360 * 24)) / 10) + ' дн. назад';
}

var getDistance = function(lat1, long1, lat2, long2) {    
    var dist = getDistanceInMeters(lat1, long1, lat2, long2); 

    var measure = 'м';
    if (dist > 3000) {
        dist /= 1000
        measure = 'км';
    }
 
    return parseInt(dist) + measure;
}
    
 var scrollToFocus = function(el) 
 {     
    var sel = el;
    if (typeof(sel) == 'undefined')
        sel = $(".focus");    
    
    if (typeof(sel) == 'undefined')
        return;
    
    var pos = sel.position();
    var list = sel.parents(".scrollable");    
    if (typeof(pos) != 'undefined' && typeof(list) != 'undefined')
    {                
        if (pos.top < 0 || pos.top + sel.outerHeight() >= list.outerHeight())
        {              
            if (pos.top < 0)
                list.scrollTop(list.scrollTop() + pos.top - list.height() + sel.height());  
            else
                list.scrollTop(list.scrollTop() + pos.top);  
            
            if (SB.platformName != 'android' || SB.platformName != 'ios' || !isSTB(SB.platformName) || !isSmartTV(SB.platformName))
                list.perfectScrollbar('update');        
        }
    }
 }


var formatDate = function(date, dayweek)
{    
    var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    
    if (dayweek == true)
        return monthToAbr(date.getMonth()) + ' ' + date.getDate() + ', ' + translator.t(days[date.getDay()]);    
    else
        return monthToAbr(date.getMonth()) + ' ' + date.getDate();    
}

var unixToDate = function(seconds)
{
    var date = new Date(seconds * 1000);
    return formatDate(date);
}

var unixToTime = function(seconds)
{
    var leftover = Math.floor(seconds) - (Math.floor(new Date().getTimezoneOffset()) * 60);
    var days = Math.floor(leftover / 86400);
    leftover = leftover - (days * 86400);
    var hours = Math.floor(leftover / 3600);
    leftover = leftover - (hours * 3600);
    var minutes = Math.floor(leftover / 60);
    var res = '';
    
    var postfix = '';
    
      
        
    if (hours < 10) res = '0';
    res += hours + ':';
    if (minutes < 10) res += '0';
    res += minutes;
    return  res + postfix;
};


var secondsToTime = function(sec)
{    
    var sec = Math.floor(sec);
    var minutes = Math.floor(sec / 60);    
    var seconds = sec - minutes * 60;
    var res = '';    
    if (minutes < 10) res += '0';
    res += minutes + ':';        
    if (seconds < 10) res += '0';
    res += seconds;
    
    return  res;
};
