
function Server() {
}

Server.signOut = function(){ 
    
    tbapp.os.makeRequest(
        tbapp.host + "logout",
        "GET",
        true,
        'json',
        function(json){},
        function(){}
    );
}

Server.updateProfile = function() {        
    
    tbapp.os.makeRequest(
        tbapp.host + "profile/update?age="+tbapp.user.age+'&status='+tbapp.user.status+'&gender='+tbapp.user.gender,
        "GET",
        true,
        'json',
        function(json){},
        function(){}
    );
}


Server.like = function(like, user_id) {      
    var url = tbapp.host + "users/"+user_id + '/';
    if (like)
        url += 'like';
    else
        url += 'dislike';

    url += '?lat=' + tbapp.lat + '&lon=' + tbapp.lon + '&ac=' + tbapp.ac;

    tbapp.os.makeRequest(
        url,
        "GET",
        true,
        'json',
        function(json){},
        function(){}
    );
}

Server.ban = function(ban, user_id) {      
    var url = tbapp.host + "users/"+user_id + '/';
    if (ban)
        url += 'ban';
    else
        url += 'unban';

    url += '?lat=' + tbapp.lat + '&lon=' + tbapp.lon + '&ac=' + tbapp.ac;

    tbapp.os.makeRequest(
        url,
        "GET",
        true,
        'json',
        function(json){},
        function(){}
    );
}

Server.updateFriendList = function() {
    
       url = tbapp.host + "users/friends";           

       url += '?lat=' + tbapp.lat + '&lon=' + tbapp.lon + '&ac=' + tbapp.ac; 
       
       tbapp.os.makeRequest(
            url,
            "GET",
            true,
            'json',
            function(json) { Server.parseFriendListResp(json); },
            function() { Server.failResp(); }
        );                       


    return;
}

Server.updateWatchList = function() {
    
       url = tbapp.host + "users/watchlist";           

       url += '?lat=' + tbapp.lat + '&lon=' + tbapp.lon + '&ac=' + tbapp.ac; 

       tbapp.os.makeRequest(
            url,
            "GET",
            true,
            'json',
            function(json) { Server.parseWatchListResp(json); },
            function() { Server.failResp(); }
        );                       


    return;
}

Server.updateData = function(init_run) {        
    
    if (getUriParam(location.href, 'auth') != 'complete') {
        loginView.show();
        return;            
    }

    if (init_run == false)
    {
        navigator.geolocation.getCurrentPosition(function(position) {
                               url = tbapp.host + "users?lat=" + tbapp.lat + '&lon=' + tbapp.lon + '&ac=' + tbapp.ac;            
                               tbapp.os.makeRequest(
                                    url,
                                    "GET",
                                    true,
                                    'json',
                                    function(json) { Server.parseResp(json,init_run); },
                                    function() { Server.failResp(); }
                                );                       
                        });

                        return;
    }

    loginView.setButtonText("Loading...", 0);            
      
    tbapp.os.makeRequest(
        tbapp.host + "profile",
        "GET",
        true,
        'json',
        function(json) { 
            if (json.user.id > 0)
            {
                tbapp.servertime = parseInt(json.server_time);
                tbapp.servertime_delta = parseInt($.now() / 1000) - tbapp.servertime;
                tbapp.user = new Array();
                tbapp.user.id = json.user.id; 
                tbapp.user.age = json.user.age;
                tbapp.user.status = json.user.status;
                tbapp.user.gender = json.user.gender;
                tbapp.user.real = json.user.real;
                tbapp.user.social_type = json.user.social_type;
                tbapp.user.ask = json.ask;
                tbapp.user.answer = json.answer;
                if (!tbapp.user.age || tbapp.user.age == 0)
                    tbapp.user.age = '';

                if (!tbapp.user.status)
                    tbapp.user.status = '';

                if (!tbapp.user.gender)
                    tbapp.user.gender = '';

                tbapp.user.firstname = json.user.first_name; 
                tbapp.user.lastname = json.user.last_name; 
                tbapp.user.avatar = json.user.avatar_100; 
                tbapp.user.avatar_big = json.user.avatar_200; 
                
                if (navigator.geolocation)
                {      
                    updateMyPosition.call(this,(function(){
                       url = tbapp.host + "users?" + 'lat=' + tbapp.lat + '&lon=' + tbapp.lon + '&ac=' + tbapp.ac;            
                       tbapp.os.makeRequest(
                            url,
                            "GET",
                            true,
                            'json',
                            function(json) { Server.parseResp(json,init_run); },
                            function() { Server.failResp(); }
                        );            
                    }));              
                               
            
                }
                else
                {
                    loginView.setButtonText("GPS IS NOT AVAILABLE", 5000);                    
                }
                
             }
             else
             {
                 loginView.show();
                 loginView.setButtonText("ERROR", 5000);                    
             }
        },
        function() { Server.failResp(); }
    );
        
    
}

Server.parseResp  = function(json, init_run) {    
    if ($.isEmptyObject(json))
    {
        loginView.show();
        loginView.setButtonText("ERROR", 5000);                
    }
    else if (typeof(json.error) != "undefined") {        
        loginView.show();
        loginView.setButtonText(json.error, 5000);                
    }
    else
    {                                    
        tbapp.user.ask = json.ask;
        tbapp.user.answer = json.answer;
                        
        if (!$.isEmptyObject(json.users))
        {                       
            tbapp.servertime = parseInt(json.server_time);
            tbapp.servertime_delta = parseInt($.now() / 1000) - tbapp.servertime;

            userList.reset();
            $.each(json.users ,function(key, el){         
                var u = new User();
                u.set({firstname: el.first_name, lastname: el.last_name, 
                    status: el.status, id: el.id, avatar: el.avatar_100, 
                    avatar_big: el.avatar_200, lat: el.lat, lon: el.lon, 
                    social_type: el.social_type, real: el.real, likes: el.likes, liked: el.liked,
                    age: el.age, gender: el.gender, update_time: el.update_time});

                if (el.social_type == '1')
                    u.set('social_url', 'http://vk.com/' + el.social_id);
                else if (el.social_type == '2')
                    u.set('social_url', 'http://facebook.com/' + el.social_id);

                userList.add(u);                     
            });
            userList.refresh();
        }        
    
        if (init_run)   
            controller.welcome(); 
        else
            controller.main(); 
    }
}

Server.parseWatchListResp  = function(json) {        
                    
    if (!$.isEmptyObject(json.users))
    {                       
        tbapp.servertime = parseInt(json.server_time);
        tbapp.servertime_delta = parseInt($.now() / 1000) - tbapp.servertime;

        watchUserList.reset();
        $.each(json.users ,function(key, el){         
            var u = new User();
            u.set({firstname: el.first_name, lastname: el.last_name, 
                status: el.status, id: el.id, avatar: el.avatar_100, 
                avatar_big: el.avatar_200, lat: el.lat, lon: el.lon, 
                real: el.real, likes: el.likes, liked: el.liked, social_type: el.social_type,
                banned: el.banned, watch_time: el.watch_time,
                age: el.age, gender: el.gender, update_time: el.update_time});

            if (el.social_type == '1')
                u.set('social_url', 'http://vk.com/' + el.social_id);
            else if (el.social_type == '2')
                u.set('social_url', 'http://facebook.com/' + el.social_id);

            watchUserList.add(u);                     
        });
        watchUserList.refresh();
    }        
    
    watchUserListView.render();         
}

Server.parseFriendListResp  = function(json) {        
                    
    if (!$.isEmptyObject(json.users))
    {                       
        tbapp.servertime = parseInt(json.server_time);
        tbapp.servertime_delta = parseInt($.now() / 1000) - tbapp.servertime;

        friendList.reset();
        $.each(json.users ,function(key, el){         
            var u = new User();
            u.set({firstname: el.first_name, lastname: el.last_name, 
                status: el.status, id: el.id, avatar: el.avatar_100, 
                avatar_big: el.avatar_200, lat: el.lat, lon: el.lon, 
                real: el.real, likes: el.likes, liked: el.liked, social_type: el.social_type,
                banned: el.banned, watch_time: el.watch_time,
                age: el.age, gender: el.gender, update_time: el.update_time});

            if (el.social_type == '1')
                u.set('social_url', 'http://vk.com/' + el.social_id);
            else if (el.social_type == '2')
                u.set('social_url', 'http://facebook.com/' + el.social_id);

            friendList.add(u);                     
        });
        friendList.refresh();
    }        
    
    friendListView.render();         
}


Server.failResp = function() {
    loginView.show();
    loginView.setButtonText("ERROR", 5000);                    
    setTimeout(Server.updateXML, 60000);
}

