var mainView = null;
(function($){    
  var MainView = Backbone.View.extend({
    el: $('#main-container'),        
    initialize: function(){
       _.bindAll(this, 'render');             
    },   
    clean: function() {
        
    },
    render: function(){        
      $("#main-container").html("");      
      var html = '<div id="side-menu-wrapper"></div><div class="main_scroll_block"></div>';
      headerView.showBackBtn(false);
      headerView.render();
      $("#main-container").append(html);                  
    }
  });  
  mainView = new MainView();
})(jQuery);

