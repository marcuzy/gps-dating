
var User = Backbone.Model.extend({
  defaults: {firstname: null, lastname: null, id: null, status: null}
});

var UserList = Backbone.Collection.extend({
  model: User,        
  refresh: function() { this.trigger('refresh'); }
});

var userList = new UserList();
var watchUserList = new UserList();
var friendList = new UserList();
var currentUser = null;




