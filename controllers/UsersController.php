<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Invite;
use app\models\InviteForm;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\DatingController;
use yii\web\Response;



/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends DatingController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }



    /**
     * Сообщить свои координаты и узнать, кто есть поблизости
     * @return mixed
     */
    public function actionIndex($test_mode=true, $lat=null, $lon=null, $ac=null)
    {
        $response = [];
        if (!Yii::$app->user->isGuest) { 
            $user = Yii::$app->user->getIdentity();
            $response = $user->findBeside();
        }

        // Yii::$app->response->format = Response::FORMAT_JSON;

        // return json_encode(['server_time'=> time(), 'users'=>$response]);
        return ['server_time'=> time(), 'users'=>$response, 'ask'=>Yii::$app->params['ask'], 'answer'=>Yii::$app->params['answer']];
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $user = User::findOne($id);
        $response = [];

        if (!is_null($user)) {
            $u = $user->responseAttrs;
            $u['liked'] = $user->isLikedByUser($this->self->id);
            $response[] = $u;
            Yii::$app->user->getIdentity()->watchTo($id);
        }

        return ['server_time'=> time(), 'users'=>$response];
    }
    
    public function actionProfile() {
        $user = Yii::$app->user->getIdentity();
        $response = [];
        if (!is_null($user)) {
            $response = $user->responseAttrs;
        }

        return ['server_time'=> time(), 'user'=>$response, 'ask'=>Yii::$app->params['ask'], 'answer'=>Yii::$app->params['answer']];
    }

    public function actionWatchlist() 
    {
        return ['server_time'=> time(), 'users'=>$this->self->watchers];
    }

    public function actionFriends() 
    {
        // $cache = Yii::$app->cache;
        // $key = 'vk_friends_'.$this->self->id;
        $friends = $this->self->friends;
        // if ($cache->exists($key))
        //     $friends = $cache->get($key);
        // else {
        //     $friends = $this->self->friends;
        //     $cache->set($key, $friends, 15);
        // }

        return ['server_time'=> time(), 'users'=>$friends];
    }

    public function actionBan($hunter_id) 
    {
        $this->self->banWatcher($hunter_id);
    }

    public function actionUnban($hunter_id) 
    {
        $this->self->unbanWatcher($hunter_id);
    }

    public function actionLike($victim_id) 
    {
        $victim = User::findOne($victim_id);

        if ($victim) {
            $victim->like($this->self->id);
            $victim->save();
        }

    }

    public function actionDislike($victim_id)
    {
        $victim = User::findOne($victim_id);

        if ($victim) {
            $victim->dislike($this->self->id);
            $victim->save();
        }
    }

    public function actionUpdate($status='', $age=0, $gender=0) 
    {

        if (Yii::$app->user->isGuest) return;
        
        $model = Yii::$app->user->getIdentity();

        $model->startCheckingSocialFields();

        $model->status = $status;
        $model->bdate = $age;
        $model->gender = $gender;

        $model->save();
    }




    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
