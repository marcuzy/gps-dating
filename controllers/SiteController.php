<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\Response;

use Facebook\FacebookRequest;
use Facebook\GraphUser;

use app\components\DatingController;
use app\components\Vk\VKException;

use app\models\Invite;
use app\models\User;


class SiteController extends DatingController
{

    public static $view = '';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
         
        ];
    }

    public function defaultResponseFormat() {
        return \yii\web\Response::FORMAT_HTML;
    }

    protected function getSignupDataFromVk() 
    {
        $data = [];

        try {
            $vk = Yii::$app->vk;

            $result = $vk->api('users.get', ['user_ids' => $user->social_id, 'fields' => 'photo_100, photo_200, sex, bdate']);
            $result = $result[0];
            
            $data = [
                'gender' => $result['sex'],
                'first_name' => $result['first_name'],
                'last_name' => $result['last_name'],
                'avatar_100' => $result['photo_100'],
                'avatar_200' => $result['photo_200']
            ];

        } catch (VKException $e){
               
           Yii::error($e->getMessage());
           $this->redirect(['/', 'auth'=>'error']);
           /*if ($e->getCode() == $vk::INCORRECT_USER_ID_ERROR) {
               $this->addErrorIncorrectId();
               return false;
           } else {
              throw new web\HttpException('Что-то пошло не так.', $e->getCode(), $e); 
           }*/
        } 

        return $data;
    }

    protected function getSignupDataFromFacebook() 
    {
        $data = [];

        try {
            $userProfile = Yii::$app->fb->api('/me', ['fields'=>'id, gender, first_name, last_name'])->getGraphObject(GraphUser::className());

            $avatar = "//graph.facebook.com/".$userProfile->getId()."/picture/?width=";

            $data = [
                'gender' => Yii::$app->fb->genderToInt($userProfile->getGender()),
                'first_name' => $userProfile->getFirstName(),
                'last_name' => $userProfile->getLastName(),
                'avatar_100' => $avatar.'100',
                'avatar_200' => $avatar.'200', 
            ];

        } catch(FacebookRequestException $e) {
            Yii::error($e->getMessage());
            $this->redirect(['/', 'auth'=>'error', 'error_msg'=>'load profile data from facebook is failed']);
        }   

        return ['data'=>$data, 'uid'=>$userProfile->getId()];
    }


    public function actionAuth($code, $social_name)
    {
        $social_id = 0;
        $access_token = '';
        // 
        
        // try {
        //     $success = Yii::$app->social->login($code, User::$socialTypes[$social_name]);
        // } catch (Exception $e) {
        //     $this->redirect(['/', 'auth'=>'error', 'debug'=>1, 'error_msg'=>$e->getMessage()]);
        // }
        
        // if ($success) {
        //     // Yii::info('login success');
        //     $this->redirect(['/', 'auth'=>'complete', 'debug'=>1]);
        // } else {
        //     $this->redirect(['/', 'auth'=>'error', 'debug'=>1, 'error_msg'=>'Login failed']);
        // }

        if ($social_name === 'vk') {
            $response = Yii::$app->vk->getAccessToken($code, Url::to(['auth/vk'], 'http'));
            if ( isset($response['user_id']) ) {
                $social_id = (string)$response['user_id'] ;
                $access_token = Yii::$app->vk->access_token;
            }
        }

        if ($social_name === 'fb') {
            $fbSession = Yii::$app->fb->createSession();
            
            if ($fbSession) {
                $fbdata = $this->getSignupDataFromFacebook();
                $social_id = $fbdata['uid'];
                $access_token = $fbSession->getToken();
             } else 
                 throw new HttpException(401, 'Не удалось получить сессию facebook');
        }
        
        $user = User::findOne(['social_id' => $social_id ]);

        if (!$user) {

            $user = new User;
            $user->social_id = $social_id;
            $user->social_type = User::$socialTypes[$social_name];
            

            if ($social_name === 'vk')
                $data = $this->getSignupDataFromVk();
            else 
                $data = $fbdata['data'];

            $user->load(['User'=>$data]);
        } 
        
        $user->access_token = $access_token;
        
        if ($user->validate()) {
            $isNewRecord = $user->isNewRecord;

            if ($user->save()) {

                //Перебор всех инвайтов для этого пользователя
                //и награждение каждого прикласившего
                if ($isNewRecord) {
                    // $invites = Invite::find()->where(['social_id' => $social_id, 'social_type' => $user->social_type])->all();
                    // // $command = Yii::$app   ->db
                    // //             ->createCommand("UPDATE `{User::tableName()}` as `u`
                    // //                                 SET `rating` = `rating`+{User::RATING_INVITE_FRIEND}
                    // //                                 WHERE `id` IN (SELECT `user_id` FROM `{Invite::tableName()}` as `i` 
                    // //                                                 WHERE i.social_id = $social_id)");
                    // // $command->execute();

                    // if ($invites)
                    //     foreach ($invites as $invite) {   
                    //         $invite->owner->addFriend($user->id);
                    //         $invite->owner->rate(User::RATING_INVITE_FRIEND);
                    //         $invite->owner->save();
                    //         $invite->delete();    
                    //     }
                    //     
                    
                    $user->rateMyFriends();

                    
                }
            }   

            Yii::info('validate success');
            if (Yii::$app->user->login($user, 3600*24*30)) {
                Yii::info('login success');
                $this->redirect(['/', 'auth'=>'complete']);
            } else {
                $this->redirect(['/', 'auth'=>'error']);
            }
        } else {
            Yii::info('login failed');
            throw new HttpException(401, json_encode($user->getErrors()) );
            //$this->redirect(['/', 'auth'=>'error', 'debug'=>1, 'error_msg'=>'validate is failed']);
        }

    }
    
    public function getAuthorizeUrl()
    {
        return Yii::$app->vk->getAuthorizeUrl('', Url::to(['auth/vk'], 'http'));
    }
    
    



    public function actionIndex($view='')
    {
    
        if (empty($auth) && !Yii::$app->user->isGuest && $_GET['auth'] != 'complete')
            $this->redirect(['/', 'auth' => 'complete']);
        
    

        // $auth = Yii::$app->user->isGuest ? '' : 'complete';
        // if ($debug==0)
        //     $this->redirect(['/', 'debug'=>1, 'view'=>$view, 'auth' => $auth]);


        return $this->renderPartial('index');
    }

    public function responseFormats()
    {
        return [
            Response::FORMAT_JSON => ['login']
        ];
    }

    public function actionLogin($social_name)
    {
        // $vkAuthUrl = $this->getAuthorizeUrl();

        // if (!\Yii::$app->user->isGuest) {
        //     return $this->goHome();
        // }
        // $cache = Yii::$app->cache;

        // if ($cache->exists('vkAuthUrl'))
        //     $vkAuthUrl = $cache->get('vkAuthUrl');
        // else
        //     $cache->set('vkAuthUrl', $this->getAuthorizeUrl(), 5);

        $social_type = User::$socialTypes[$social_name];

        $this->redirect( $social_type === User::SOCIAL_VK ? Yii::$app->vk->getAuthorizeUrl('', Url::to(['auth/vk'], 'http')) : Yii::$app->fb->getLoginUrl());

        // return $this->render('login', [
        //         'vkAuthUrl' => $vkAuthUrl,
        //         'fbAuthUrl' => Yii::$app->fb->getLoginUrl()
        //     ]);
    }

    public function actionLogout()
    {
        
        if (!Yii::$app->user->isGuest) {
 
            

            $user = $this->self;
            if ($user) {
                $user->access_token = '';
                //$user->auth_key = '';
                $user->save();
            }

            Yii::$app->user->logout();
        }

        return $this->goHome();
    }



    public function getAuthUrl() {
        return Yii::$app->vk->getAuthorizeUrl('', yii\helpers\Url::to(['site/auth']));
    }
}
