-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 22 2015 г., 15:31
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.5.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `gps`
--

-- --------------------------------------------------------

--
-- Структура таблицы `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `social_type` tinyint(2) NOT NULL COMMENT 'id соц сети',
  PRIMARY KEY (`user_id`,`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `invite_friends`
--

CREATE TABLE IF NOT EXISTS `invite_friends` (
  `user_id` int(11) NOT NULL,
  `social_id` int(11) NOT NULL,
  `social_type` tinyint(2) NOT NULL,
  PRIMARY KEY (`user_id`,`social_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `social_id` varchar(24) DEFAULT NULL,
  `access_token` text,
  `social_type` tinyint(2) DEFAULT NULL,
  `first_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `status` varchar(512) DEFAULT NULL,
  `avatar_100` varchar(512) DEFAULT NULL,
  `avatar_200` varchar(512) DEFAULT NULL,
  `bdate` int(11) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT '0' COMMENT '1-male, 2-female,0-none',
  `lat` double DEFAULT '0',
  `lon` double DEFAULT '0',
  `ac` double DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `shared` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'поделился ли в соц сети',
  `real` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'статус "реальный"',
  `friends_count` int(11) NOT NULL DEFAULT '0' COMMENT 'колво друзей',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Структура таблицы `watchers`
--

CREATE TABLE IF NOT EXISTS `watchers` (
  `hunter_id` int(11) NOT NULL COMMENT 'наблюдатель',
  `victim_id` int(11) NOT NULL COMMENT 'жертва',
  `watch_time` int(11) NOT NULL COMMENT 'когда последний раз наблюдал',
  `banned` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'запрещено ли охотнику наблюдать за жертвой',
  `liked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`hunter_id`,`victim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='кто за кем наблюдает';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
