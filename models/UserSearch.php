<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vk_id', 'project_id', 'role_id', 'use_user_vk_id', 'messages_total', 'messages_day', 'blocked'], 'integer'],
            [['first_name', 'last_name', 'signup_timestamp', 'vk_access_token', 'auth_key', 'photo_100', 'photo_200', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'vk_id' => $this->vk_id,
            'project_id' => $this->project_id,
            'role_id' => $this->role_id,
            'use_user_vk_id' => $this->use_user_vk_id,
            'messages_total' => $this->messages_total,
            'messages_day' => $this->messages_day,
            'blocked' => $this->blocked,
            'signup_timestamp' => $this->signup_timestamp,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'vk_access_token', $this->vk_access_token])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'photo_100', $this->photo_100])
            ->andFilterWhere(['like', 'photo_200', $this->photo_200])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
