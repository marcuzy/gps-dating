<?php

namespace app\models;

use Yii;
use app\models\User;

/**
 * Связывает "охотника"" и его "жертву"
 */
class Watcher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%watchers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hunter_id', 'victim_id', 'watch_time'], 'required'],
            [['hunter_id', 'victim_id', 'watch_time'], 'integer'],
            [['banned', 'liked'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hunter_id' => 'Hunter',
            'victim_id' => 'Victim',
            'watch_time' => 'Watch Time',
            'banned' => 'Is Banned',
            'liked' => 'Liked',
        ];
    }

    public function ban() 
    {
        $this->banned = true;
    }

    public function unban() 
    {
        $this->banned = false;
    }

    public function like() 
    {
        $this->liked = true;
    }

    public function dislike() 
    {
        $this->liked = false;
    }

    public function getHunter() 
    {
        return $this->hasOne( User::className(), ['id'=>'hunter_id'] );
    }

    public function getVictim() 
    {
        return $this->hasOne( User::className(), ['id'=>'victim_id'] );
    }


}
