<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%friends}}".
 *
 * @property integer $user_id
 * @property integer $friend_id
 * @property integer $social_type
 */
class Friend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%friends}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'friend_id', 'social_type'], 'required'],
            [['user_id', 'friend_id', 'social_type'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'friend_id' => 'Friend ID',
            'social_type' => 'Social Type',
        ];
    }
}
