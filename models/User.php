<?php

namespace app\models;

use Yii;
use app\models\Watcher;
use app\models\Friend;
use yii\db\Query;
use Facebook\GraphUser;
use app\components\Helper;


class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface 
{

    const RADIUS_DEFAULT_VALUE = 0.83333; //50 секунд = 50/60
    const OFFLINE_TIME = 600; //через сколько секунд считать пользователя оффлайн

    //очки рейтинга
    const RATING_INVITE_FRIEND = 10;
    const RATING_SOCIAL_FIELD = 5; //gender, age, status
    const RATING_SOMEBODY_LIKE_ME = 1;

    const SOCIAL_VK = 1;
    const SOCIAL_FB = 2;

    //ограничение на выдачу из бд
    const ROWS_PER_PAGE = 1000;

    public static $socialTypes = [
        'vk' => self::SOCIAL_VK ,
        'fb' => self::SOCIAL_FB,
    ];

    public static $responseFields = [
        'id', /*'gender',*/ 'likes', 'rating', /*'bdate',*/ 'update_time', 'lat', 'lon', 'ac', 'first_name', 'last_name',
        'status', 'avatar_100', 'avatar_200', 'social_type', 'social_id', 'real'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    public $isModelSaved = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['social_id', 'access_token', 'social_name', 'first_name', 'last_name', 'status', 'avatar', 'bdate', 'gender', 'lat', 'lon', 'ac', 'update_time'], 'required'],
            [['gender', 'likes', 'rating', 'social_type', 'friends_count'], 'integer'],
            [['access_token', 'social_id'], 'string'],
            [['bdate', 'update_time'], 'safe'],
            [['lat', 'lon', 'ac',], 'number'],
            [['first_name', 'last_name'], 'string', 'max' => 128],
            [['social_id'], 'string', 'max' => 24],
            [['status', 'avatar_100', 'avatar_200'], 'string', 'max' => 512],
            [['shared', 'real'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'social_id' => 'Social ID',
            'access_token' => 'Access Token',
            'social_name' => 'Social Name',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'status' => 'Status',
            'avatar' => 'Avatar',
            'bdate' => 'Bdate',
            'gender' => 'Gender',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'ac' => 'Ac',
            'update_time' => 'Update Time',
        ];
    }

    // public function getSocialUrl()
    // {
    //     if ($this->social_type === self::SOCIAL_VK)
    //         return "https://vk.com/id".$this->social_id;
    //     else
    //         return "https://facebook.com/".$this->social_id;
    // }

    public function getResponseAttrs()
    {
        // $r = $this->attributes;
        // $r['age'] = $this->bdate;      
        
        // $r['social_url'] = $this->socialUrl;
        
        // unset($r['access_token']);
        

        return $this->getAttributes(self::$responseFields);  
    }

    
    /**
     * Поиск людей поблизости
     * @param  int $size радиус поиска в градусах
     * @return array   
     */
    public function findBeside($size = self::RADIUS_DEFAULT_VALUE) 
    {
        //longitude latitude
        //границы поиска
        $lonA = (float) ($this->lon - $size);
        $lonB = (float) ($this->lon + $size);
        $latA = (float) ($this->lat - $size);
        $latB = (float) ($this->lat + $size);

        $query = new Query;

        $query  ->select(array_merge(self::$responseFields, ['liked']))
                ->from(User::tableName())
                ->leftJoin(Watcher::tableName(), "`hunter_id` = {$this->id} AND `victim_id` = `id`")
                ->where(['between', 'lon', $lonA, $lonB])
                ->andWhere(['between', 'lat', $latA, $latB])
                ->andWhere(['<>', 'id', $this->id])
                ->andWhere(['<', '(UNIX_TIMESTAMP() - update_time)', self::OFFLINE_TIME])
                ->andWhere('`banned` = 0 OR ISNULL( `banned` )')
                ->orderBy('(lat*lat+lon*lon) ASC')
                ->limit(self::ROWS_PER_PAGE);

        $command = $query->createCommand();
        $users = $command->queryAll();
        
        return $users;
        // return $command->getRawSql();
    }


    ///
        /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        // return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        return User::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::findOne(['access_token' => $token]);
    }

     /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->last_name;
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->access_token;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->access_token === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return true;
    }

    /**
     * Наблюдать за пользователем $victimId
     * @param  int
     * @return void
     */
    public function watchTo($victimId) 
    {
        $watcher = $this->findOneVictim($victimId);

        if (!$watcher) {
            $watcher = new Watcher();
            $watcher->hunter_id = $this->id;
            $watcher->victim_id = $victimId;
        }

        $watcher->watch_time = time();
        $watcher->save();
    }

    private $_watchers = [];

    /**
     * Кто наблюдает за мной
     * @return array
     */
    public function getWatchers() 
    {  
        if (empty($this->_watchers)) {
            $query = new Query;

            $fields = ['banned', 'liked', 'watch_time'];
   
            $fields = array_merge(self::$responseFields, $fields);

            $query  ->select($fields)
                    ->from(Watcher::tableName())
                    ->innerJoin(User::tableName(), '`id` = `hunter_id` AND `victim_id` = '.$this->id)
                    ->where(['victim_id' => $this->id])
                    ->orderBy(['watch_time' => SORT_DESC, 'rating' => SORT_DESC])
                    ->limit(self::ROWS_PER_PAGE);

            $this->_watchers = $query->createCommand()->queryAll();
        }

        return $this->_watchers;
    }

    protected function getSocialFriends()
    {
        $ids = [];
        if ($this->isVk) {
            $ids = Yii::$app->vk->api('friends.getAppUsers');

        } else { //facebook
            $result = Yii::$app->fb->api('/me/friends', ['fields'=>'id'])->getGraphObject()->asArray();
            
            if ($result && $result['data']){
                foreach ($result['data'] as $obj) {
                    $ids[] = $obj->id;
                }
            }
        }

        return $ids;
    }

    public function getFriends()
    {
        $friends = [];
        $ids = $this->getSocialFriends();
        
        //ищем друзей в базе приложения
        if (!empty($ids)) {
            $query = new Query;
            
            // $fields = $idOnly ? ['id'] : self::$responseFields;

            $query  ->select(self::$responseFields)
                    ->from(User::tableName())
                    ->where(['in', 'social_id', $ids])
                    ->andWhere(['social_type'=>$this->social_type])
                    ->orderBy(['rating' => SORT_DESC])
                    ->limit(self::ROWS_PER_PAGE);

            $friends = $query->createCommand()->queryAll();
        }

        return $friends;
    }

    /**
     * Наградить тех моих друзей, кто репостил приглашение в соц сети @see User::shared
     * @return [type] [description]
     */
    public function rateMyFriends()
    {
        $ids = $this->getSocialFriends();

        if (!empty($ids)){
            $ids = implode(',', $ids);
            $cmd = Yii::$app->db->createCommand('UPDATE '.self::tableName().' SET `rating`=`rating`+:points, `friends_count`= `friends_count`+1, `real`=IF(`friends_count`>=3, 1, 0) WHERE social_id IN ('.$ids.') AND `shared`=1',

                [':points'=>self::RATING_INVITE_FRIEND]);

            // var_dump($cmd->getRawSql());
            // die();
            $cmd->execute();
        }
    }

    /**
     * Запретить пользователю $hunterId видеть меня в списке @see User::findBeside
     * @param  int
     * @return void
     */
    public function banWatcher($hunterId) 
    {
        $watcher = $this->findOneHunter($hunterId);

        if ($watcher) {
            $watcher->ban();
            $watcher->save();
        }
    }

    public function unbanWatcher($hunterId) 
    {
        $watcher = $this->findOneHunter($hunterId);

        if ($watcher) {
            $watcher->unban();
            $watcher->save();
        }
    }

    /**
     * Меня лайкнул пользователь $hunterId.
     * @param  int
     * @return void
     */
    public function like($hunterId) 
    {
        $watcher = $this->findOneHunter($hunterId);

        if ($watcher && !$watcher->liked){
            $this->likes++;
            $this->rate( self::RATING_SOMEBODY_LIKE_ME );
            $watcher->like();
            $watcher->save();
        }
    }

    public function dislike($hunterId) 
    {
        $watcher = $this->findOneHunter($hunterId);
        
        if ($watcher && $watcher->liked) {
            $this->likes--;
            $this->rate( -self::RATING_SOMEBODY_LIKE_ME );
            $watcher->dislike();
            $watcher->save();  
        }  
    }

    /**
     * Ставил ли мне лайк юзер с id $hunterId
     * @param  int
     * @return boolean
     */
    public function isLikedByUser($hunterId) 
    {
        $watcher = $this->findOneHunter($hunterId);

        return $watcher && $watcher->liked;
    }

    /**
     * Запретил ли я пользователю с id $hunterId следить за мной
     * @param  int
     * @return boolean
     */
    public function isBannedUser($hunterId) 
    {
        $watcher = $this->findOneHunter($hunterId);

        return $watcher && $watcher->banned;
    }

    private $_victimes = [];
    public function findOneVictim($victimId) 
    {
        if (!isset($this->_victimes[$victimId]))
            $this->_victimes[$victimId] = Watcher::findOne(['hunter_id'=>$this->id, 'victim_id'=>$victimId]);

        return $this->_victimes[$victimId];
    } 

    private $_hunters = [];
    public function findOneHunter($hunterId)
    {
        if (!isset($this->_hunters[$hunterId]))
            $this->_hunters[$hunterId] = Watcher::findOne(['hunter_id'=>$hunterId, 'victim_id'=>$this->id]);

        return $this->_hunters[$hunterId];
    }

    /**
     * Начсиление рейтинга
     * @param  int
     * @return void
     */
    public function rate($points) 
    {
        $this->rating += $points;
    }

    /**
     * Клон $this
     * @var User
     */
    private $_clone = null;

    /**
     * Создает дубликат объекта, чтобы потом сравнить изменения,
     * отслеживание изменнеий важно для начисления рейтинга
     * @return void
     */
    public function startCheckingSocialFields() 
    {
        $this->_clone = clone $this;
    }

    /**
     * Отслеживает изменения заданых полей и начисляет рейтинг
     * @return void
     */
    protected function rateForSocialFields() {
        if ($this->_clone) {
            $fields = ['status', 'gender', 'bdate'];

            foreach ($fields as $field) {
                if (Helper::isEmptyToFill( $this->_clone->$field, $this->$field ))
                    $this->rate( self::RATING_SOCIAL_FIELD );
                if (Helper::isfillToEmpty( $this->_clone->$field, $this->$field ))
                    $this->rate( -self::RATING_SOCIAL_FIELD );
            }

        }
    }

    public function beforeSave($insert) 
    {
        $this->update_time = time();
        
        if (parent::beforeSave($insert)) {

            if ($this->_clone)
                $this->rateForSocialFields();
            
            return true;
        } else {
            return false;
        }

    }

    // public function setGPS($lat, $lon, $ac)
    // {
    //     if (!is_null($lat))
    //         $this->lat = $lat;

    //     if (!is_null($lon))                   
    //         $this->lon = $lon;
        
    //     if (!is_null($ac))
    //         $this->ac = $ac;
    // }

    // public function addFriend($userId) {
    //     $friend = new Friend;
    //     $friend->user_id = $this->id;
    //     $friend->friend_id = $userId;
    //     $friend->social_type = $this->social_type;
    //     $friend->save();
    // }

    public function getIsVk() {
        return (boolean)($this->social_type === self::SOCIAL_VK);
    }

    public function getIsFb() {
        return $this->social_type === self::SOCIAL_FB;
    }

    // public function isMyFriend($firendId) {
    //     return self::find()->where(['friend_id'=>$friendId, 'user_id'=>$this->id])->exists();
    // }


    public function afterSave( $insert, $changedAttributes )
    {
        $this->isModelSaved = true;
    }

}
