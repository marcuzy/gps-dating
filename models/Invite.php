<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%invite_friends}}".
 *
 * @property integer $user_id
 * @property integer $social_id
 * @property integer $social_type
 */
class Invite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%invite_friends}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'social_id', 'social_type'], 'required'],
            [['user_id', 'social_id', 'social_type'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'social_id' => 'Social ID',
            'social_type' => 'Social Type',
        ];
    }

    public function getOwner() {
        return $this->hasOne( User::tableName(), ['id'=>'user_id'] );
    }


}
