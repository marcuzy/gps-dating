<?php

namespace app\commands;

use yii\console\Controller;


class CorrectController extends Controller
{

	private function getDbName(){
		$dsn = explode(';', \Yii::$app->db->dsn);
		$dbNamePare = explode('=', end($dsn));
		return end($dbNamePare); 
	}

	public function actionIndex($file)
	{
		echo "Processing sql file..\n";
		$path = \Yii::getAlias($file);
	
		$content = file_get_contents($path);

		$alterRegexp = '/alter table ([\w]+) add constraint ([\w]+) foreign key \(([\w]+)\)[\s]+references ([\w]+) \(([\w]+)\)( on delete restrict on update restrict;)/';
		$createTableRegexp = "/create table ([\w]+)\n\(\n([^;]+)\n\);/i";

		$content = preg_replace_callback($createTableRegexp, function($matches){
			$tableName = $matches[1];
			$tableDef = $matches[2];

			//удаление префикса у атрибутов таблицы, в качестве префикса - имя таблицы
			$tableDef = preg_replace("/([\s]+){$tableName}_([\w]+)([\s]+[^,]+)(,|[\s]+)/", "$1`$2` $3$4", $tableDef);

			//удаление префикса в выражении primary key, в качестве префикса - имя таблицы
			$tableDef = preg_replace("/([\s]+primary key[\s]+)\({$tableName}_([\w]+)\)/", "$1($2)\n", $tableDef);

			return "CREATE TABLE `$tableName`\n(\n$tableDef\n);\n\n";

		}, $content);

		//после того, как атрибуты таблиц были изменены, придется изменить все конструкции
		//alter table, создающие связи м/д таблицами
		$content = preg_replace_callback($alterRegexp, function($matches){
			array_shift($matches);

			list($alterTableName, $constraintName, $foKey, $refTable, $refKey, $rigth) = $matches;

			//удаление префикса для подчиненной таблици
			$refKey = preg_replace("/^{$refTable}_([\w]+)/", "$1", $refKey);
			//удаление префикса для главной таблицы
			$foKey = preg_replace("/^{$alterTableName}_([\w]+)/", "$1", $foKey);

			return "alter table `$alterTableName` add constraint $constraintName foreign key (`$foKey`) references `$refTable` (`$refKey`)".$rigth;
		}, $content);

		echo $this->getDbName() ;
		$content = "use ". $this->getDbName() . ';';
		$content .= "SET FOREIGN_KEY_CHECKS = 0;\n".$content;
		$content .= "SET FOREIGN_KEY_CHECKS = 1;\n";

		file_put_contents($path.'_converted.sql', $content);
		echo "Done.\n";

		echo "Import to mysql..\n";
		system("mysql -u root -p < {$path}_converted.sql");
		echo "Done.\n";
			

		// $tableDefs = [];
		// $alterRestricts = [];
		// $createTablesCount = preg_match_all($createTableRegexp, $content, $createTableMatches, PREG_SET_ORDER);
		
		// if (!empty($createTablesCount)){
		// 	foreach ($createTableMatches as $createTableMatch) {
		// 		array_shift($createTableMatch);
		// 		list($tableName, $tableDef) = $createTableMatch;
		// 		$tableDefs[$tableName] = $tableDef; 
		// 	}
		// }

		// $alterAddRestrictCount = preg_match_all($alterRegexp, $content, $alterAddRestrictMatches, PREG_SET_ORDER);

		// if (!empty($alterAddRestrictCount)){
		// 	foreach ($alterAddRestrictMatches as $match) {
		// 		array_shift($match);
		// 		$hash = [];
		// 		$hash = array_combine(['alterTable', 'constraintName', 'foreignKey', 'refTable', 'refKey'], $match);
		// 		// list($hash['alterTable'], $hash['foreignKey'], $hash['refTable'], $hash['refKey']) = $match;
		// 		$alterRestricts[$hash['refTable']][$hash['refKey']][] = $hash;
		// 	}
		// }

		// array_walk($tableDefs, function(&$tableDef, $tableName) use(&$alterRestricts){
		// 	if (isset($alterRestricts[$tableName]))
		// 		$restrict = &$alterRestricts[$tableName];

		// 	$tableAttrRegexp = '/([\w]+)[\s]+[^,]+(,|[\s]+)/';
		// 	$attrsCount = preg_match_all($tableAttrRegexp, $tableDef, $matches, PREG_SET_ORDER);


		// 	if (strpos('primary key ',  end($matches)[0])!==false){
		// 		preg_match('/primary key[\s]*\([\w]\)/', subject)
		// 		array_pop($matches);
		// 	}


		// 	$attrs = array_map(function($match){
		// 		return $match[1];
		// 	}, $matches);


		// 	echo "==$tableName==\n";
		// 	foreach ($attrs as $attr) {
		// 		$newAttrName = $attr;
		// 		if (strpos($attr, $tableName.'_') !==false)
		// 			$newAttrName = substr($attr, strlen($tableName)+1);

		// 		echo "$attr = $newAttrName\n";

		// 		if (isset($restrict[$attr])){
		// 			$restrict[$attr] = array_map(function($hash) use($newAttrName){
		// 				$hash['refKey'] = $newAttrName;
		// 				return $hash;
		// 			},$restrict[$attr]); 
		// 		}

		// 		// $tableDef = str_replace($attr, $newAttrName, $tableDef);
		// 		$tableDef = preg_replace("/[\s]+($attr)[\s]+/", "\n\t`".$newAttrName.'` ', $tableDef);
		// 	}


		// });

		// $createTablesSql = '';
		// $dropTablesSql = '';
		// array_walk($tableDefs, function($tableDef, $tableName) use(&$createTablesSql, &$dropTablesSql){
		// 	$createTablesSql .= "CREATE TABLE $tableName\n(\n$tableDef\n);\n\n";
		// 	$dropTablesSql .= "DROP TABLE IF EXISTS `$tableName`;\n\n";
		// });

		// echo $createTablesSql;
		// $alterTablesSql = '';
		// foreach ($alterRestricts as $restr) {
		// 	foreach ($restr as $key) {
		// 		foreach ($key as $atom) {
		// 			$alterTablesSql .= vsprintf("alter table `%s` add constraint `%s` foreign key (`%s`) references `%s` (`%s`) on delete restrict on update restrict;\n\n", $atom);
		// 		}
		// 	}
		// }

		// echo $alterTablesSql;

		// $sql = "SET FOREIGN_KEY_CHECKS = 0;\n\n"; //игнонировать связи при удалении таблиц
		// $sql .= $dropTablesSql.$createTablesSql;
		// $sql .= "SET FOREIGN_KEY_CHECKS = 1;\n\n";
		// $sql .= $alterTablesSql;

		// //rename($path, $path);
		// file_put_contents($path.'_converted.sql', $sql);



	}
	
}